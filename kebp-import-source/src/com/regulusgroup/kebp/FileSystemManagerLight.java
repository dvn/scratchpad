/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.regulusgroup.kebp;

import java.util.Properties;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import javax.crypto.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.codec.DecoderException;


/**
 *
 * @author kaushika
 */
public class FileSystemManagerLight {

    // This class is meant for creation of the file system hash during import routines.
    // This class is NOT multi-threaded.

    // Store the root directory of the file system. Top Level Directory.
    // Path Created is as per function "getSavePath()"
    private File RootDir; 
    private String sep = File.separator;
    SimpleLog log = new SimpleLog();
    // Controls number of files per dir by controlling the dir length.
    private int DirCount=3;
    // Controls the number of folders (depth) created.
    private int DirDepth=4; 
    long sand = 1; // Used to randomize the MD5 algorithm.
    Crypto crypto = null;

    
    FileSystemManagerLight()
        throws DecoderException, InvalidKeyException, IOException,
        NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
        InvalidAlgorithmParameterException
    {
    	long t0 = System.currentTimeMillis();
        loadConfig(); // Load Configuration File
    	long t1 = System.currentTimeMillis();
        crypto = new Crypto();
    	long t2 = System.currentTimeMillis();
    	System.out.println("loadConfig:"+(t1-t0));
    	System.out.println("crypto:"+(t2-t1));
    }

    private void loadConfig()
        throws IOException
    {

        // Load variables from property file.
        Properties props = new Properties();
        props.load(new BufferedInputStream(new FileInputStream("FileSystemManager.properties")));
        RootDir = new java.io.File((props.getProperty("RootDir")).toString());
        DirCount = Integer.parseInt((props.getProperty("DirCount")).toString());
        DirDepth = Integer.parseInt((props.getProperty("DirDepth")).toString());
    }

//    public boolean fileCopy (String fromFilename, String toFilename ) {
//
//        boolean success = false;
//        // Get File name from file.
//        File fromFile = new File ( fromFilename );
//        if (! fromFile.exists() ) { return false; }
//
//        // Save the file on the file system.
//        File toFile= new File(toFilename);
//        File toDir = new File(toFile.getParent());
//        toDir.mkdirs();
//
//        // If file does not exist, create file
//        FileInputStream from = null;
//        FileOutputStream to = null;
//        byte[] buffer = new byte[4096];
//        try {
//            from = new FileInputStream(fromFile);
//            to = new FileOutputStream(toFile);
//            int bytesRead;
//            while ((bytesRead = from.read(buffer)) != -1) {
//                to.write(buffer, 0, bytesRead); // write
//            }
//            success = true;
//        } catch (IOException e) {
//            e.printStackTrace();
//            success = false;
//        } finally {
//            if (from != null) {
//                try {
//                    from.close();
//                } catch (IOException e) {
//                }
//            }
//            if (to != null) {
//                try {
//                    to.close();
//                } catch (IOException e) {
//                }
//            }
//        }
//
//        // For extra security, don't leave any plaintext hanging around memory.
//        java.util.Arrays.fill(buffer, (byte) 0);
//
//        return success;
//    }

//    private String getMD5 ( String str ) {
//        MessageDigest md5=null;
//        byte[] hash=null;
//        StringBuffer sb = new StringBuffer();
//        try {
//            md5 = MessageDigest.getInstance("MD5");
//            md5.update(str.getBytes("UTF-8"));
//            hash = md5.digest();
//            for (int i =0; i<hash.length; i++ ) {
//                String tmpStr = "0"+Integer.toHexString( (0xff & hash[i]));
//                sb.append(tmpStr.substring(tmpStr.length()-2));
//            }
//            // System.out.println(sb.toString());
//        } catch ( Exception e) {}
//        return sb.toString();
//    }

//    private String convertMD5toPath ( String filename ) {
//        // Generate the MD5
//        // Note that the same filename will generate the same MD5.
//        // Use randomization with timestamp ts and incremental sand if that is an issue.
//        // long ts = System.currentTimeMillis();
//        // String filepath = getMD5(filename + ts + sand++);
//        // Even better - let the application apply the randomization.
//        String filepath = getMD5(filename);
//        char[] c1 = filepath.toCharArray();
//        String s2 = "";
//        int count = 0;
//        int depth = 1;
//
//        // Insert the file separators.
//        for (int i=0 ; i< c1.length; i++ ) {
//            count++;
//            s2 = s2 + c1[i];
//            // if ((i==pathcut1) || (i==pathcut2) || (i==pathcut3) || (i==pathcut4)) {
//            if (((count % DirCount) == 0 ) && (depth <= DirDepth) ){
//                depth++;
//                s2 = s2 + sep;
//            }
//        }
//        return s2;
//    }

//    private String getYYYYMM () {
//        // Generate YYYYMM format for monthly dir.
//        // Support some optimization on mount points during deployment
//        // to reduce contention for disks (using mount points etc).
//        Calendar now = Calendar.getInstance();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMM");
//        // SimpleDateFormat formatter = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
//        return formatter.format(now.getTime());
//    }
//
//    private String getFileExtension (String filename) {
//        // Extensions are not used on all operating systems.
//        File tmpFile = new File (filename);
//        String name = tmpFile.getName();
//        int whereDot = tmpFile.getName().lastIndexOf('.');
//        if (0 < whereDot && whereDot <= tmpFile.getName().length() - 2 ) {
//            return "." + tmpFile.getName().substring(whereDot+1);
//        }
//        return "";
//    }
    
//    public String getSavePath ( String filename ) {
//        // Returns path to save DIRECTORY ONLY.
//        // Consider changing the filename to random as well?
//        String filepath =
//                RootDir.getAbsolutePath()
//                + sep
//                + getYYYYMM()
//                + sep
//                + convertMD5toPath( filename )
//                + getFileExtension( filename );
//        return filepath;
//    }

//    public boolean fileMove (String oldFilePath, String newFilePath) {
//        File oldFile = new File (oldFilePath);
//        File newFile = new File (newFilePath);
//        boolean success = false;
//        success = oldFile.renameTo(newFile);
//        if (!success) {
//            log.write(log.ERROR, "Failed to move file [" +
//                oldFilePath + "] to [" +
//                newFilePath + "]");
//        }
//        return success;
//    }
//
//    public boolean secureDelete(String filename) throws IOException {
//
//        File file = new File(filename);
//        if (file.exists()) {
//            SecureRandom random = new SecureRandom();
//            RandomAccessFile raf = new RandomAccessFile(file, "rw");
//            FileChannel channel = raf.getChannel();
//            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, raf.length());
//            // overwrite with zeros
//            while (buffer.hasRemaining()) {
//                buffer.put((byte) 0);
//            }
//            buffer.force();
//            buffer.rewind();
//            // overwrite with ones
//            while (buffer.hasRemaining()) {
//                buffer.put((byte) 0xFF);
//            }
//            buffer.force();
//            buffer.rewind();
//            // overwrite with random data; one byte at a time
//            byte[] data = new byte[1];
//            while (buffer.hasRemaining()) {
//                random.nextBytes(data);
//                buffer.put(data[0]);
//            }
//            buffer.force();
//            file.delete();
//        } else {
//            return false;
//        }
//        return true;
//    }
//
//    public boolean secureDelete(File file) throws IOException {
//        if (file.exists()) {
//            long length = file.length();
//            SecureRandom random = new SecureRandom();
//            RandomAccessFile raf = new RandomAccessFile(file, "rws");
//            raf.seek(0);
//            raf.getFilePointer();
//            byte[] data = new byte[64];
//            int pos = 0;
//            while (pos < length) {
//                random.nextBytes(data);
//                raf.write(data);
//                pos += data.length;
//            }
//            raf.close();
//            file.delete();
//            return true;
//        }
//        return false;
//    }

//    public boolean encryptAndSave (FileInputStream in, FileOutputStream out)
//            throws NoSuchAlgorithmException, InvalidKeyException,
//            NoSuchPaddingException, IOException
//         
//    {
//        boolean success = false;
//        success = crypto.encrypt(in, out);
//        return success;
//    }

//    public boolean decryptAndRead (FileInputStream in, FileOutputStream out)
//            throws NoSuchAlgorithmException, InvalidKeyException,
//            NoSuchPaddingException, IOException, IllegalBlockSizeException, 
//            BadPaddingException 
//    {
//        boolean success = false;
//        success = crypto.decrypt(in, out);
//        return success;
//    }
//
//    public boolean encryptAndSave (File infile, File outfile)
//            throws NoSuchAlgorithmException, InvalidKeyException,
//            NoSuchPaddingException, IOException
//    {
//        boolean success = false;
//        FileInputStream in = new FileInputStream(infile);
//        File dir = new File(outfile.getParent());
//        dir.mkdirs();
//        FileOutputStream out = new FileOutputStream(outfile);
//        // Copy from one stream to another.
//        success = crypto.encrypt(in, out);
//        return success;
//    }
//
//    public boolean decryptAndRead(File infile, File outfile)
//            throws NoSuchAlgorithmException, InvalidKeyException,
//            NoSuchPaddingException, IOException, IllegalBlockSizeException,
//            BadPaddingException {
//        boolean success = false;
//        FileInputStream in = new FileInputStream(infile);
//        File dir = new File(outfile.getParent());
//        dir.mkdirs();
//        FileOutputStream out = new FileOutputStream(outfile);
//        success = crypto.decrypt(in, out);
//        return success;
//    }
//
//    public boolean encryptAndSave (String infile, String outfile)
//            throws NoSuchAlgorithmException, InvalidKeyException,
//            NoSuchPaddingException, IOException
//    {
//        boolean success = false;
//        // Copy from one stream to another.
//        success = this.encryptAndSave(new File(infile), new File(outfile));
//        return success;
//    }
//
//    public boolean decryptAndRead(String infile, String outfile)
//            throws NoSuchAlgorithmException, InvalidKeyException,
//            NoSuchPaddingException, IOException, IllegalBlockSizeException,
//            BadPaddingException {
//        boolean success = false;
//        success = this.decryptAndRead(new File(infile), new File(outfile));
//        return success;
//    }

    public byte[] decryptAndRead(String infile)
    throws NoSuchAlgorithmException, InvalidKeyException,
    NoSuchPaddingException, IOException, IllegalBlockSizeException,
    BadPaddingException {
        FileInputStream in = new FileInputStream(infile);
        byte[] buffer = crypto.decrypt(in);
		return buffer;
	}
    

    public byte[] getKey() {
        return this.crypto.getKey();
    }

}
