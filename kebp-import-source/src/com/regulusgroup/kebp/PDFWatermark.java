package com.regulusgroup.kebp;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class PDFWatermark {
    private File preprintjpg; //Pre-printed stationery background

    // For pdfWatermark
    private PdfReader reader = null;
    private PdfStamper stamper = null;
    private int no_pgs = 0;
    private Image watermark;
    private PdfLayer watermark_printed = null;
    private SimpleLog log = new SimpleLog();

    /**
     * Constructor
     */
    PDFWatermark() {
        this.loadconfig();
    }

    private void loadconfig() {

        Properties props = new Properties();
        try {
            // Load properties file
            props.load(new FileInputStream("pdfutil.properties"));

            // Load variables from property file.
            preprintjpg = new java.io.File((props.getProperty("PdfWatermarkPreprintJPG")).toString());
            log.write( log.INFO, "Using preprint file: " + preprintjpg.toString());

        } //catch exception in case properties file does not exist
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] pdfWatermark(byte[] inBytes) {
        // Insert a watermark in the PDF document.
    	ByteArrayOutputStream baos = null;
        try {
            reader = new PdfReader(inBytes);
            no_pgs = reader.getNumberOfPages();

            watermark = Image.getInstance(preprintjpg.toString());
            // Create a stamper that will copy the document to a new file
            baos = new ByteArrayOutputStream();
            stamper = new PdfStamper(reader, baos);

            watermark_printed = new PdfLayer("printed", stamper.getWriter());
            watermark_printed.setOn(false);
            watermark_printed.setOnPanel(false);
            watermark_printed.setPrint("print", true);

            for (int i = 0; i < stamper.getReader().getNumberOfPages();) {
                PdfContentByte cb = stamper.getUnderContent(++i);
                Rectangle rectangle = stamper.getReader().getPageSizeWithRotation(i);
                
                float AbsoluteX = rectangle.getLeft();
                float AbsoluteY = rectangle.getBottom();

                watermark.setAbsolutePosition(AbsoluteX, AbsoluteY);
                cb.addImage(watermark);
                
                // In case you need to add to layer, uncomment line below.
                // cb.endLayer();
            }
            stamper.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

}
