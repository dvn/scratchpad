/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.regulusgroup.kebp;

import java.io.BufferedReader;
import java.io.FileReader;
// import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.HashMap;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * FWParser, reads setup from pdfutils.properties
 * Takes a fixed-width text file and tokenizes and returns a String[]
 * loadFile
 * readline (in loop)
 * closeFile
 * @author kaushika
 */
public class FWParser {

    private String[] cn = null;
    private int[] co = null;
    private int[] cl = null;
    private int NoOfCols = 0;
    private int NoOfHRows = 0;
    private int NoOfHCols = 0;
    private String[] hcn = null;
    private int[] hco = null;
    private int[] hcl = null;
    private BufferedReader in = null;
    private SimpleLog log = null;
    private String config_file = null;
    private String data_file = null;
    private int ParseHeader_YN = 0;
    private Map<String, HashMap<String, String>> controlMap = new TreeMap<String, HashMap<String, String>>(); 
    
    FWParser(String conffile) {
        this.log = new SimpleLog();
        this.config_file = conffile;
        this.loadconfig(conffile);
    }

    private void loadconfig(String config_file) {

        Properties props = new Properties();
        try {
            // Load properties file
            props.load(new BufferedInputStream(new FileInputStream(config_file)));

            // Check how many columns defined for Header Row
            NoOfHCols = Integer.parseInt(props.getProperty("FWPNoOfHeaderCols"));
            NoOfHRows = Integer.parseInt(props.getProperty("FWPNoOfHeaderRows"));
            hcn = new String[NoOfHCols];
            hco = new int[NoOfHCols];
            hcl = new int[NoOfHCols];
            for (int i = 0; i < NoOfHCols; i++) {
                // Get the variables.
                String istr = String.valueOf(i);
                hcn[i] = (props.getProperty("FWPHColName" + istr)).toUpperCase();
                hco[i] = Integer.parseInt(props.getProperty("FWPHOffset" + istr));
                hcl[i] = Integer.parseInt(props.getProperty("FWPHLength" + istr));
                log.write(log.DEBUG, "Name[" + hcn[i] + "] Offset[" + hco[i] + "] Length[" + hcl[i] + "]");
            }

            // Check how many columns have been defined for Data Rows
            NoOfCols = Integer.parseInt(props.getProperty("FWPNoOfParseCols"));
            cn = new String[NoOfCols];
            co = new int[NoOfCols];
            cl = new int[NoOfCols];

            for (int i = 0; i < NoOfCols; i++) {
                // Get the variables.
                String istr = String.valueOf(i);
                cn[i] = (props.getProperty("FWPColName" + istr)).toUpperCase();
                co[i] = Integer.parseInt(props.getProperty("FWPOffset" + istr));
                cl[i] = Integer.parseInt(props.getProperty("FWPLength" + istr));
                log.write(log.DEBUG, "Name[" + cn[i] + "] Offset[" + co[i] + "] Length[" + cl[i] + "]");
            }

        } //catch exception in case properties file does not exist
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadFile(String file_to_be_parsed, int SkipHeaderRows_YN) {
        // Need to load all lines in txt file
        String throw_away = null;
        data_file = file_to_be_parsed;

        // Check if file is already loaded.
        if (in == null) {
            // If file is not loaded, load file.
            try {
                in = new BufferedReader(new FileReader(data_file));
                // Advance the rows in case there are Header Rows.
                if (SkipHeaderRows_YN > 0) {
                    for (int i = 0; i < NoOfHRows; i++) {
                        // Throw away the line.
                        throw_away = in.readLine();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // If file is loaded, then RELOAD file.
            reloadFile(SkipHeaderRows_YN);
        }
        return;
    }

    public void closeFile() {
        // If file is open, close it.
        if (!(in == null)) {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
    }

    public void reloadFile(int SkipHeaderRows_YN) {
        String throw_away = null;
        // If file is open, close it.
        if (!(in == null)) {
            try {
                in.close();
            } catch (Exception e) {
            }
        }
        // Open the file again.
        try {
            in = new BufferedReader(new FileReader(data_file));
            // Advance the rows in case there are Header Rows.
            if (SkipHeaderRows_YN > 0) {
                for (int i = 0; i < NoOfHRows; i++) {
                    // Throw away the line.
                    throw_away = in.readLine();
                }
            }
        } catch (Exception e) {
        }
    }

    public HashMap readDataLine() {
        HashMap tokens = null;
        String str = null;
        int offset = 0;
        int length = 0;
        int endcut = 0;
        int strlen = 0;
        try {
            if (in == null) {
                log.write(log.ERROR, "FWParser does not have data file loaded; call loadFile(file) first");
            } else if ((str = in.readLine()) != null) {
                // Start Parsing Data Line.
                strlen = str.length();
                if (strlen == 0) {
                    return null;
                }
                tokens = new HashMap();
                for (int i = 0; i < NoOfCols; i++) {
                    // Get the lengths from array
                    offset = co[i];
                    length = cl[i];
                    endcut = offset + length;

                    if (endcut > strlen) {
                        // update cutend to be until end of string.
                        endcut = strlen;
                    }
                    if (offset > strlen) {
                        offset = strlen;
                    }
                    // Construct a substring match
                    tokens.put(cn[i], str.substring((offset-1), (endcut-1)));
                }
            }
        } catch (IOException e) {
        }
        return tokens;
    }

   public HashMap readHeaderLine() {
        HashMap tokens = null;
        String str = null;
        int offset = 0;
        int length = 0;
        int endcut = 0;
        int strlen = 0;
        try {
            if (in == null) {
                log.write(log.ERROR, "FWParser does not have data file loaded; call loadFile(file) first");
            } else if ((str = in.readLine()) != null) {
                // Start Parsing Data Line.
                strlen = str.length();
                if (strlen == 0) {
                    return null;
                }
                tokens = new HashMap();
                for (int i = 0; i < NoOfHCols; i++) {
                    // Get the lengths from array
                    offset = hco[i];
                    length = hcl[i];
                    endcut = offset + length;

                    if (endcut > strlen) {
                        // update cutend to be until end of string.
                        endcut = strlen;
                    }
                    if (offset > strlen) {
                        offset = strlen;
                    }
                    // Construct a substring match
                    tokens.put(hcn[i], str.substring((offset-1), (endcut-1)));
                }
            }
        } catch (IOException e) {
        }
        return tokens;
    }

    public void printTokens(HashMap tokens) {
        Set set = tokens.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            System.out.println("Key ["+me.getKey() + "]: Value[" + me.getValue()+ "]");
        }
    }

    public HashMap trimHashMap(HashMap tokens) {
        // Assumes that all parsed values are strings.
        HashMap hm = new HashMap();
        Set set = tokens.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            hm.put(((String)me.getKey()), ((String)me.getValue()).trim());
        }
        return hm;
    }
    
    public void addRow(String eidkey, HashMap<String, String> row){
    	controlMap.put(eidkey, row);
    }
    
    public HashMap<String, String> getRow(String eidkey){
    	return controlMap.get(eidkey);
    }
    
}
