/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.regulusgroup.kebp;

import java.io.PrintWriter;
import java.io.FileOutputStream;

/**
 *
 * @author kaushika
 */
public class SimpleLog {
    /** Debug levels **/
    public final int DEBUG =5;
    public final int INFO  =4;
    public final int WARN  =3;
    public final int ERROR =2;
    public final int FATAL =1;
    public final int SILENT=0;
    private static int DEBUGLEVEL=3;

    SimpleLog () {
        DEBUGLEVEL=3;
    }

    public void setDEBUGLEVEL(int DEBUGLEVEL) {
        this.DEBUGLEVEL = DEBUGLEVEL;
    }

    public int getDEBUGLEVEL() {
        return DEBUGLEVEL;
    }

    public void write (int loglvl, String str) {
        String caller = new Throwable().getStackTrace()[1].getClassName();
        if(DEBUGLEVEL < loglvl) {
            System.out.println("Caller[" + caller + "] LOGLVL[" + loglvl + "]:" + str);
        }
    }
}
