package com.regulusgroup.kebp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipAllFiles {

    /**
     * @param args
     */
    public void unzipFiles() {
        Properties props = new Properties();
        try {
            props.load(new BufferedInputStream(new FileInputStream(
                    "pdfutil.properties")));
            String dpsDir = props.getProperty("DpsDir");
            String unzipDir = props.getProperty("UnzipDir");
            new File(unzipDir).mkdirs();
            File dir = new File(dpsDir);

            String[] children = dir.list();
            if (children == null) {
                System.out.println("Not a directory");
                // Either dir does not exist or is not a directory
            } else {
                for (int i = 0; i < children.length; i++) {
                    // Get filename of file or directory
                    String filename = children[i];
                    try {
                        BufferedOutputStream out = null;
                        ZipInputStream in = new ZipInputStream(new BufferedInputStream(new FileInputStream(dpsDir + "/" + filename)));
                        ZipEntry entry;
                        while ((entry = in.getNextEntry()) != null) {
                            int count;
                            byte data[] = new byte[1000];
                            out = new BufferedOutputStream(new FileOutputStream(unzipDir + "/" + entry.getName()), 1000);
                            while ((count = in.read(data, 0, 1000)) != -1) {
                                out.write(data, 0, count);
                            }
                            out.flush();
                            out.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Clean up
        }
        System.out.println("Done extracting...");

    }
}
