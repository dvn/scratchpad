package com.regulusgroup.kebp;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

/**
*
* @author ajay krishnamurthy
*/


public class DataBaseConnectionUtil {
	private static DataSource dataSource = getDataSource(); 
	public static final String DB_PROPERTIES = "database.properties";

	public static DataSource getDataSource() {
		Properties properties = new Properties();
		BasicDataSource ds = null;
		try {
			properties.load(new BufferedInputStream(new FileInputStream(
					DB_PROPERTIES)));
			ds = new BasicDataSource();
			ds.setDriverClassName(properties.getProperty("driverClassName"));
			ds.setUsername(properties.getProperty("userName"));
			ds.setPassword(properties.getProperty("password"));
			ds.setUrl(properties.getProperty("url"));
			ds.setMaxActive(Integer.parseInt(properties
					.getProperty("maxActive")));
			ds.setMaxIdle(Integer.parseInt(properties.getProperty("maxIdle")));
			properties = null;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ds;
	}

	public static Connection getConnection() throws SQLException {
		//DataSource dataSource = getDataSource();
//		Connection connection = null;
//		try {
//			if (dataSource != null) {
//				connection = dataSource.getConnection();
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		return dataSource.getConnection();
	}

}
