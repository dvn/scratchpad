/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.regulusgroup.kebp;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author kaushika
 */
public class RecordReference {

    String acct_num;
    String eid_key;
    String st_dte;
    HashMap HeaderTokens;
    HashMap RowTokens;
    String save_path;
    /** Crypto doesn't work with Strings. Remember to retain this as a byte array **/
    /** When saving this key to a database, will need to save correctly as a memory buffer?? **/
    // Pros of DB Storage
    // On the positive side, saving key to DB record means that each file can have it's own key.
    // If we loose the shadow file, even by mistake, we're basically screwed - the files cannot be decrypted at all.
    // (so don't loose it) - Good case for system backups.
    // If one key is compromised, all of the keys are compromised.
    // Cons:
    // The key we have is stored physically somewhere in the DB instead of being in the shadow file
    // If we loose a DB backup - or extract, then the keys go out the door.
    // Loss of one key affects only that record.
    byte[] crypto_key; // Should we generate and save the secret key separately for every file?


    public RecordReference() {
    }

    public RecordReference(String acct_num, String eid_key, String st_dte,
            HashMap HeaderTokens, HashMap RowTokens,
            String save_path, byte[] crypto_key) {
        this.acct_num = acct_num;
        this.eid_key = eid_key;
        this.st_dte = st_dte;
        this.HeaderTokens = HeaderTokens;
        this.RowTokens = RowTokens;
        this.save_path = save_path;
        crypto_key = new byte[16];
        System.arraycopy(crypto_key, 0, this.crypto_key, 0, 16);
    }

    public HashMap getHeaderTokens() {
        return HeaderTokens;
    }

    public void setHeaderTokens(HashMap HeaderTokens) {
        this.HeaderTokens = HeaderTokens;
    }

    public HashMap getRowTokens() {
        return RowTokens;
    }

    public void setRowTokens(HashMap RowTokens) {
        this.RowTokens = RowTokens;
    }

    public String getAcct_num() {
        return acct_num;
    }

    public void setAcct_num(String acct_num) {
        this.acct_num = acct_num;
    }

    public byte[] getCrypto_key() {
        return crypto_key;
    }

    public void setCrypto_key(byte[] crypto_key) {
        this.crypto_key = crypto_key;
    }

    public String getEid_key() {
        return eid_key;
    }

    public void setEid_key(String eid_key) {
        this.eid_key = eid_key;
    }

    public String getSave_path() {
        return save_path;
    }

    public void setSave_path(String save_path) {
        this.save_path = save_path;
    }

    public String getSt_dte() {
        return st_dte;
    }

    public void setSt_dte(String st_dte) {
        this.st_dte = st_dte;
    }

    public String toString() {
        return "RRObj[acct_num=" + acct_num + ",eid_key=" + eid_key + ",st_dte=" + st_dte + ",save_path=" + save_path +
                ",crypto_key=" + crypto_key.toString() +
                ",HeaderTokens=" + (!(this.HeaderTokens==null)) +
                ",RowTokens=" + (!(this.RowTokens==null)) + 
                "]";
    }

    public void printAll() {
        System.out.println(this.toString());
        System.out.println("HEADER-TOKENS");
        printHashMap(HeaderTokens);
        System.out.println("ROW-TOKENS");
        printHashMap(RowTokens);
    }

    public void printSummary() {
        System.out.println(this.toString());
    }

    private void printHashMap(HashMap tokens) {
        if(!( tokens == null) ){
            Set set = tokens.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                System.out.println("Key ["+me.getKey() + "]: Value[" + me.getValue()+ "]");
            }
        } else  {
            System.out.println("Empty HashMap");
        }
    }
}
