package com.regulusgroup.kebp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.regulusgroup.kebp.model.UserInfo;

/**
 * 
 * @author ajay krishnamurthy
 */
public class DataPersister {

	private static final Logger log = Logger.getLogger(DataPersister.class);
	// private static Connection connection = null;
	// private static Statement stmt = null;
	private static final Integer MODIFIED_BY = -1;
	private static String sep = File.separator;
	private static final int LAST_NAME_SEARCH_LENGTH = 5;
	private static int LIMIT_RECORD_COUNT = 0;
	private static Properties props = null;
	private static String nameDelimeter = "";

	public DataPersister() throws IOException, SQLException {
		props = new Properties();
		try {
			props.load(new BufferedInputStream(new FileInputStream(
					"email_updator.properties")));
			LIMIT_RECORD_COUNT = Integer.valueOf(props
					.getProperty("LimittedRecordCount"));
			props = new Properties();
			props.load(new BufferedInputStream(new FileInputStream(
					"fwp.properties")));
			nameDelimeter = props.getProperty("NameDelimeter");
			if (nameDelimeter == null || nameDelimeter.equals("")) {
				nameDelimeter = " ";
			}

		} catch (FileNotFoundException e) {
			log.error(e);
			throw e;
		} catch (IOException e) {
			log.error(e);
			throw e;
		}
		// connection = DataBaseConnectionUtil.getConnection();
		// stmt = connection.createStatement();
	}

	public boolean saveRecordReference(RecordReference recordReference)
			throws Exception {
		// System.out.println(" Inside saveRecordReference");
		boolean isBillSaved = false;
		try {
			HashMap tokens = recordReference.getRowTokens();
			String mrnNumber = (String) tokens.get("MRN_NUM");
			String mrnPrefix = (String) tokens.get("MRN_PREFIX");
			String fullName = (String) tokens.get("FULL_NAME");
			String pastDue = (String) tokens.get("PAST_DUE");
			String billDate = new SimpleDateFormat("yyyy-mm-dd")
					.format(new SimpleDateFormat("mm/dd/yyyy")
							.parse((String) tokens.get("BILL_DATE")));
			String buId = recordReference.getAcct_num();
			String[] nameArr = fullName.trim().split(nameDelimeter);
			String firstName = "";
			for (int i = 0; i <= nameArr.length - 2; i++) {
				if (i == 0) {
					firstName = nameArr[i];
				} else {
					firstName = firstName + " " + nameArr[i];
				}
			}
			String lastName = nameArr[nameArr.length - 1];
			log.trace("Fullname:" + fullName);

			if (lastName!=null && lastName.trim().equals("")) {
				log.error("Last Name is blank... Ignoring record...:"
						+ recordReference.eid_key + " " + fullName + ", "
						+ recordReference.getSave_path());
				return false;
			}

			if (!firstName.matches("^[a-z A-Z-.']+$")
					|| !lastName.matches("^[a-z A-Z-.']+$")) {
				log
						.error("Name contains special characters... Ignoring record...:"
								+ recordReference.eid_key
								+ " "
								+ fullName
								+ ", " + recordReference.getSave_path());
				return false;
			}
			String partialLastName = lastName.length() > LAST_NAME_SEARCH_LENGTH ? lastName
					.substring(0, LAST_NAME_SEARCH_LENGTH)
					: lastName;
			// Replace ' in the name with mysql escape sequence
			// firstName = firstName.replace("'", "\\'");
			// lastName = lastName.replace("'", "\\'");
			// partialLastName = partialLastName.replace("'", "\\'");
			// Get the existing userInfo Id for the
			// buid and mrn combination
			Integer userInfoId = getUserInfoId(mrnNumber, mrnPrefix,
					partialLastName);
			Integer userAccountId = null;
			if (userInfoId == null) {
				saveUserInfo(mrnNumber, mrnPrefix, firstName, lastName);
			} else {
				// update the existing first name with the new first name
				updateUserInfo(firstName, userInfoId);
			}
			boolean mailSentUpdated = updateMailStatus(mrnNumber, mrnPrefix,
					partialLastName, 0);
			userAccountId = getUserAccountId(userInfoId, buId);
			if (userAccountId == null) {
				saveUserAccount(mrnNumber, mrnPrefix, partialLastName, buId);
			}
			isBillSaved = saveOrUpdateUserBill(mrnNumber, mrnPrefix,
					partialLastName, recordReference, buId, pastDue, billDate);
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
		return isBillSaved;

	}

	public boolean saveOrUpdateUserBill(String mrnNumber, String mrnPrefix,
			String partialLastName, RecordReference recordReference,
			String buid, String pastDue, String billDate) throws SQLException {
		// System.out.println(" Inside saveOrUpdateUserBill");
		HashMap tokens = recordReference.getRowTokens();
		StringBuilder sql = new StringBuilder();
		sql
				.append(" select count(*) as rowCount from user_bill where user_account_id=");
		// to get the user Account Id on the basis of
		// mrn,mrnPrefix and the partial Last name
		sql.append(" (select ID from user_account where ");
		sql.append(" user_info_id=");
		sql.append(" ( select id  from user_info where mrn=? ");
		sql.append(" and mrn_prefix=? ");
		sql.append(" and substring(last_name,1,5)=? ) and buid=").append(buid)
				.append(" )");

		sql.append(" and bill_date='").append(billDate).append("' ");

		String savedFileName = recordReference.getSave_path();
		String billDueDate = tokens.get("BILL_DUE_DATE").toString();
		String fileName = "";
		String folderLocation = "";
		if (savedFileName != null && !savedFileName.equals("")) {
			fileName = savedFileName.substring(savedFileName.lastIndexOf(sep)
					+ sep.length(), savedFileName.length());
			folderLocation = savedFileName.substring(0, savedFileName
					.lastIndexOf(sep));
		}
		try {
			Connection conn = DataBaseConnectionUtil.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql.toString());
			stmt.setLong(1, Long.valueOf(mrnNumber));
			stmt.setString(2, mrnPrefix);
			stmt.setString(3, partialLastName);

			ResultSet rs = stmt.executeQuery();
			Integer rowCount = 0;
			while (rs.next()) {
				rowCount = rs.getInt("rowCount");
			}
			String countSQL = sql.toString();
			sql = new StringBuilder();
			if (rowCount > 0) {
				// Already exists just Update the values
				sql.append(" update user_bill set amount_billed=").append(
						tokens.get("AMOUNT_DUE"));
				sql.append(" ,file_location='").append(folderLocation).append(
						"'");
				sql.append(" ,file_name='").append(fileName).append("' ");
				if (billDueDate != null && !billDueDate.equals("")) {
					sql.append(",bill_due_date='").append(billDueDate).append(
							"'");
				}
				if (pastDue != null && pastDue.equals("PAST DUE")) {
					sql.append(",past_due=").append("0x01");
				} else {
					sql.append(",past_due=").append("0x00");
				}
				sql.append(" where user_account_id=");
				// to get the user Account Id on the basis of
				// mrn,mrnPrefix and the partial Last name
				sql.append(" (select ID from user_account where user_info_id=");
				sql.append(" ( select id  from user_info where mrn=? ");
				sql.append(" and mrn_prefix=? ");
				sql.append(" and substring(last_name,1,5)=? ) and buid=")
						.append(buid).append(" )");

				sql.append(" and bill_date='").append(billDate).append("' ");

				log.warn("Existing record found in database based on key..."
						+ sql);
				log.warn("Updating record with..." + sql);

			} else {
				// insert new row into user_bill
				sql
						.append(" insert into user_bill(user_account_id,amount_billed,bill_date,");
				sql.append(" file_location,file_name");
				if (billDueDate != null && !billDueDate.equals("")) {
					sql.append(",bill_due_date");

				}
				sql.append(",past_due");
				sql.append(" ) values (");
				// to get the user Account Id on the basis of
				// mrn,mrnPrefix and the partial Last name
				sql.append(" (select ID from user_account where user_info_id=");
				sql.append(" ( select id  from user_info where mrn=? ");
				sql.append(" and mrn_prefix=? ");
				sql.append(" and substring(last_name,1,5)=? ) and buid=")
						.append(buid).append(" )");
				sql.append(",");
				sql.append(tokens.get("AMOUNT_DUE")).append(",");
				sql.append("'").append(billDate).append("',");
				sql.append("'").append(folderLocation).append("',");
				sql.append("'").append(fileName).append("'");
				if (billDueDate != null && !billDueDate.equals("")) {
					sql.append(",'").append(billDueDate).append("'");
				}
				if (pastDue != null && pastDue.equals("PAST DUE")) {
					sql.append(",").append("0x01");
				} else {
					sql.append(",").append("0x00");
				}
				sql.append(")");
				log.trace("Inserting data into database:" + sql.toString());

			}
			String strSql = sql.toString();
			strSql = strSql.replace("\\", "\\\\");
			// System.out.println("SQL>>" + sql.toString());
			if (rowCount == 0) {
				// batchUpdateList.addBatch(strSql);
				PreparedStatement preparedStatement = conn
						.prepareStatement(strSql);
				preparedStatement.setLong(1, Long.valueOf(mrnNumber));
				preparedStatement.setString(2, mrnPrefix);
				preparedStatement.setString(3, partialLastName);
				preparedStatement.execute();
				preparedStatement.close();
				// stmt.executeUpdate(strSql);
				// statement.executeUpdate(strSql);
			} else {
				// batchUpdateList.addBatch(strSql);
				PreparedStatement preparedStatement = conn
						.prepareStatement(strSql);
				preparedStatement.setLong(1, Long.valueOf(mrnNumber));
				preparedStatement.setString(2, mrnPrefix);
				preparedStatement.setString(3, partialLastName);

				preparedStatement.execute();
				preparedStatement.close();
				// stmt.executeUpdate(strSql);
				// statement.executeUpdate(strSql);
				log
						.warn("Skipping update record, MRN, MRN_PREFIX & LAST_NAME duplicate. [Key] MRN:"
								+ (String) tokens.get("MRN_NUM")
								+ ", MRN_PREFIX:"
								+ (String) tokens.get("MRN_PREFIX")
								+ ", FULL_NAME:"
								+ (String) tokens.get("FULL_NAME")
								+ ". [New Data] File Location: "
								+ folderLocation
								+ ", FileName:"
								+ fileName
								+ ":: SQL:" + sql.toString());
			}
			rs = null;
			stmt.close();
			conn.close();
		} catch (SQLException sqlEx) {
			log.error("The Error is :::" + sqlEx);
			throw sqlEx;
		}

		return true;
	}

	public Integer getUserInfoId(String mrnNumber, String mrnPrefix,
			String partialLastName) throws SQLException {

		// System.out.println(" Inside getUserInfoId");
		// check for the existance of mrn and buid in the DB
		Integer userInfoId = null;
		StringBuilder sql = new StringBuilder();
		sql.append(" select id as userInfoId from user_info where mrn=? ");
		sql.append(" and mrn_prefix=? ");
		sql.append(" and substring(last_name,1,5)=? ");
		try {
			Connection conn = DataBaseConnectionUtil.getConnection();
			PreparedStatement preparedStatement = conn.prepareStatement(sql
					.toString());
			preparedStatement.setLong(1, Long.valueOf(mrnNumber));
			preparedStatement.setString(2, mrnPrefix);
			preparedStatement.setString(3, partialLastName);

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				userInfoId = rs.getInt("userInfoId");
			}
			// notify the statement and rs for garbage Collection
			rs = null;
			preparedStatement.close();
			conn.close();
		} catch (SQLException sqlEx) {
			log.error(sqlEx);
			throw sqlEx;
		}
		return userInfoId;
	}

	public Integer getUserAccountId(Integer userInfoId, String buId)
			throws SQLException {
		// System.out.println(" Inside getUserAccountId");
		Integer userAccountId = null;
		StringBuilder sql = new StringBuilder();
		sql
				.append(
						" select ID as userAccountId from user_account where user_info_id=")
				.append(userInfoId);
		sql.append(" and buid=").append(buId);

		try {
			Connection conn = DataBaseConnectionUtil.getConnection();
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				userAccountId = rs.getInt("userAccountId");
			}
			// notify the statement and rs for garbage Collection
			rs = null;
			stmt.close();
			conn.close();
		} catch (SQLException sqlEx) {
			log.error(sqlEx);
			throw sqlEx;
		}
		return userAccountId;
	}

	public boolean saveUserInfo(String mrnNumber, String mrnPrefix,
			String firstName, String lastName) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(" insert into user_info (mrn, mrn_prefix,");
		sql.append("first_name, last_name) values (?,?,?,?)");
		// batchUpdateList.addBatch(sql.toString());
		Connection conn = DataBaseConnectionUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, Long.valueOf(mrnNumber));
		ps.setString(2, mrnPrefix);
		ps.setString(3, firstName);
		ps.setString(4, lastName);
		ps.execute();
		ps.close();
		conn.close();
		return true;

	}

	public boolean updateMailStatus(String mrnNumber, String mrnPrefix,
			String partialLastName, Integer status) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(" update user_info set email_sent=").append(status);
		sql.append(" where mrn=? ");
		sql.append(" and mrn_prefix=? ");
		sql.append(" and substring(last_name,1,5)=? ");
		// batchUpdateList.addBatch(sql.toString());
		Connection conn = DataBaseConnectionUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, Long.valueOf(mrnNumber));
		ps.setString(2, mrnPrefix);
		ps.setString(3, partialLastName);

		ps.execute();
		/*
		 * Statement stmt = conn.createStatement();
		 * stmt.executeUpdate(sql.toString()); stmt.close();
		 */
		ps.close();
		conn.close();
		return true;
	}

	public boolean updateMailStatus(Integer userInfoId, Integer status) {
		// System.out.println(" Inside saveUserInfo");
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" update user_info set email_sent=? where id=?");
			Connection conn = DataBaseConnectionUtil.getConnection();

			PreparedStatement ps = conn.prepareStatement(sql.toString());
			ps.setInt(1, status);
			ps.setInt(2, userInfoId);
			ps.executeUpdate();
			sql = null;
			ps.close();
			conn.close();
		} catch (SQLException sqlEx) {
			log.error(sqlEx);
			return false;
		}
		return true;

	}

	public boolean saveUserAccount(String mrnNumber, String mrnPrefix,
			String partialLastName, String buId) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("insert into `user_account` ");
		sql
				.append(" (`modified_by`,`modified_on`,`paper_opt_out`,`user_info_id`,`buid`)");
		sql.append(" VALUES (");
		sql.append(MODIFIED_BY).append(",NOW(),0x00,");

		sql.append(" ( select id  from user_info where mrn=? ");
		sql.append(" and mrn_prefix=? ");
		sql.append(" and substring(last_name,1,5)=? ) ");
		sql.append(",").append(buId).append(")");
		// batchUpdateList.addBatch(sql.toString());
		Connection conn = DataBaseConnectionUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement(sql.toString());
		ps.setLong(1, Long.valueOf(mrnNumber));
		ps.setString(2, mrnPrefix);
		ps.setString(3, partialLastName);

		ps.execute();
		ps.close();
		conn.close();
		return true;
	}

	public boolean updateUserInfo(String firstName, Integer userInfoId)
			throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(" update user_info set first_name=? where id=").append(
				userInfoId);
		// batchUpdateList.addBatch(sql.toString());
		Connection conn = DataBaseConnectionUtil.getConnection();
		PreparedStatement stmt = conn.prepareStatement(sql.toString());
		stmt.setString(1, firstName);
		stmt.executeUpdate();
		stmt.close();
		conn.close();
		return true;
	}

	public boolean updateUserInfoEmails(List<UserInfo> userInfoList)
			throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql
				.append(" update user_info set email=? where mrn=? and mrn_prefix = ? and substring(last_name,1,5)=? ");
		try {
			Connection conn = DataBaseConnectionUtil.getConnection();

			PreparedStatement ps = conn.prepareStatement(sql.toString());
			for (UserInfo userInfo : userInfoList) {
				String lastName = userInfo.getLastName();
				String partialLastName = lastName.length() <= LAST_NAME_SEARCH_LENGTH ? lastName
						: lastName.substring(0, LAST_NAME_SEARCH_LENGTH);
				ps.setString(1, userInfo.getEmail());
				ps.setLong(2, userInfo.getMrn());
				ps.setString(3, userInfo.getMrnPrefix());
				ps.setString(4, partialLastName);
				ps.executeUpdate();
			}
			ps.close();
			conn.close();
		} catch (SQLException e) {
			log.error(e);
			throw e;
		}
		return true;
	}

	public List<UserInfo> getEmailPendingList() throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select	userInfo.email as emailTo,");
		sql.append(" 		userInfo.id as userInfoId,");
		sql.append(" 		userInfo.first_name as firstName,");
		sql.append(" 		userInfo.mrn as mrnNumber,");
		sql.append(" 		userInfo.buid as buiId,");
		sql.append(" 		userInfo.last_name as lastName, ");
		sql.append(" 		userAccount.id as accountId, ");
		sql.append("        userAccount.paper_opt_out as paperOptOut ");
		sql.append(" from    user_account userAccount,user_info userInfo ");
		sql.append(" where   userAccount.user_info_id = userInfo.id");
		sql.append("     and userInfo.email_sent=0	");
		sql.append("     and userInfo.email is not null ");
		sql.append(" order by userAccount.modified_on desc ");
		sql.append(" limit ").append(LIMIT_RECORD_COUNT);
		// log.debug("Query is ::" + sql.toString());
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		try {
			Connection conn = DataBaseConnectionUtil.getConnection();
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql.toString());
			while (rs.next()) {
				UserInfo userInfo = new UserInfo();
				userInfo.setId(rs.getLong("userInfoId"));
				userInfo.setEmail(rs.getString("emailTo"));
				userInfo.setFirstName(rs.getString("firstName"));
				userInfo.setLastName(rs.getString("lastName"));
				userInfo.setMrn(Long.valueOf(rs.getString("mrnNumber")));
				userInfo.setBuid(rs.getString("buiId"));
				userInfo.setUserAccountId(rs.getLong("accountId"));
				userInfo.setPaperOptOut(rs.getString("paperOptOut"));
				userInfoList.add(userInfo);
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			log.error(e);
			throw e;
		}

		return userInfoList;
	}

	public boolean saveEmailLog(UserInfo userInfo) throws SQLException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("insert into `email_log` ");
			sql
					.append(" (`email_uuid`,`recipient_name`,`recipient_email`,`recipient_mrn`,`recipient_buid`,`recipient_account_id`)");
			sql.append(" VALUES (?,?,?,?,?,?) ");
			Connection conn = DataBaseConnectionUtil.getConnection();

			PreparedStatement ps = conn.prepareStatement(sql.toString());
			ps.setString(1, userInfo.getUuid());
			ps.setString(2, userInfo.getFullName());
			ps.setString(3, userInfo.getEmail());
			ps.setString(4, userInfo.getMrn().toString());
			ps.setString(5, userInfo.getBuid());
			ps.setString(6, userInfo.getUserAccountId().toString());
			ps.executeUpdate();
			sql = null;
			ps.close();
			conn.close();
		} catch (SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}
		return true;
	}

	// public void getRowCount() throws SQLException {
	// try {
	//
	// StringBuilder sql = new StringBuilder();
	// sql
	// .append(" select * from user_info where uuid='7c03160f-849d-417a-a0f2-447c9d385a9c'");
	// Statement st = connection.createStatement();
	// ResultSet rs = st.executeQuery(sql.toString());
	//
	// } catch (SQLException sqlEx) {
	// log.debug(sqlEx);
	// throw sqlEx;
	// }
	// }

	// public void addBatchQuery(String sql) {
	// batchUpdateList.addBatch(sql);
	//
	// }

	// public void clearUpdateList() throws SQLException {
	// stmt.clearBatch();
	// }

	// public void executeBatchUpdate() throws Exception {
	// //Statement stmt = null;
	// try {
	// //connection.setAutoCommit(true);
	// //stmt = connection.createStatement();
	//
	// // for (String sql : batchUpdateList) {
	// // stmt.addBatch(sql);
	// // }
	// int[] updateCounts = stmt.executeBatch();
	// checkUpdateCounts(updateCounts);
	// //connection.commit();
	// log.debug("Update Counts are :" + updateCounts.length);
	// } catch (BatchUpdateException e) {
	// int[] updateCounts = e.getUpdateCounts();
	// log.error("*************** Exception :"+updateCounts.length);
	// log.error(e.getMessage());
	// e.printStackTrace();
	// try {
	// //connection.rollback();
	// } catch (Exception exception) {
	// log.error(exception);
	// }
	// throw e;
	// } catch (Exception exception) {
	// log.error(exception);
	// throw exception;
	// // } finally {
	// // try {
	// // stmt.close();
	// // } catch (SQLException exception) {
	// // log.debug(exception);
	// // }
	// }
	// }

	// public void closeConnections() throws SQLException{
	// stmt.close();
	// connection.close();
	// }

	// public void checkUpdateCounts(int[] updateCounts) {
	// for (int i = 0; i < updateCounts.length; i++) {
	// if (updateCounts[i] >= 0) {
	// // Successfully executed; the number represents number of affected rows
	// log.debug("OK: updateCount=" + updateCounts[i]);
	// } else if (updateCounts[i] == Statement.SUCCESS_NO_INFO) {
	// // Successfully executed; number of affected rows not available
	// log.debug("OK: updateCount=Statement.SUCCESS_NO_INFO");
	// } else if (updateCounts[i] == Statement.EXECUTE_FAILED) {
	// log.error("updateCount["+i+"]=Statement.EXECUTE_FAILED");
	// }
	// }
	// }

	public boolean wasSkippedLastTime(String mrnNumber, String mrnPrefix,
			String fullName, boolean skipRecord,
			boolean processOnlyAllowedSpecChars, boolean processOnlyMorethanOne)
			throws SQLException {
		if (!skipRecord) {
			return true;
		}

		if (processOnlyAllowedSpecChars && !fullName.matches("^[a-z A-Z,]+$")) {
			/*
			 * log.error("Name:" + fullName + ",MRN:" + mrnNumber +
			 * ",MRN Prefix:" + mrnPrefix + " Contains Special characters");
			 */
			return true;
		}
		if (processOnlyMorethanOne) {
			String[] nameArr = fullName.trim().split(nameDelimeter);
			String firstName = "";
			for (int i = 0; i <= nameArr.length - 2; i++) {
				if (i == 0) {
					firstName = nameArr[i];
				} else {
					firstName = firstName + " " + nameArr[i];
				}
			}
			String lastName = nameArr[nameArr.length - 1];
			String partialLastName = lastName.length() > LAST_NAME_SEARCH_LENGTH ? lastName
					.substring(0, LAST_NAME_SEARCH_LENGTH)
					: lastName;
			if (!partialLastName.equals("%")) {
				partialLastName = partialLastName + "%";
			}
			StringBuilder sql = new StringBuilder();
			sql
					.append(" select count(*) as rowCount  from user_info where mrn=? ");
			sql.append(" and mrn_prefix=?");
			sql.append(" and last_name like ? ");
			try {
				Connection connection = DataBaseConnectionUtil.getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(sql.toString());
				preparedStatement.setLong(1, Long.valueOf(mrnNumber));
				preparedStatement.setString(2, mrnPrefix);
				preparedStatement.setString(3, partialLastName);

				ResultSet rs = preparedStatement.executeQuery();
				int rowCount = 0;
				while (rs.next()) {
					rowCount = rs.getInt("rowCount");
				}
				rs.close();
				preparedStatement.close();
				connection.close();
				if (partialLastName.length() < (LAST_NAME_SEARCH_LENGTH + 1)
						&& rowCount > 1) {
					/*
					 * log.error("Name:" + fullName + ",MRN:" + mrnNumber +
					 * ",MRN Prefix:" + mrnPrefix +
					 * " Contains Duplicate Entry");
					 */
					return true;
				}
			} catch (SQLException e) {
				log.error("SQLException=" + e);
				throw e;
			}
		}
		return false;
	}

	public boolean isBillMissed(String savedFileName) throws SQLException {
		boolean returnValue = false;
		String fileName = "";
		String folderLocation = "";
		if (savedFileName != null && !savedFileName.equals("")) {
			fileName = savedFileName.substring(savedFileName.lastIndexOf(sep)
					+ sep.length(), savedFileName.length());
			folderLocation = savedFileName.substring(0, savedFileName
					.lastIndexOf(sep));
		}

		StringBuilder sql = new StringBuilder();
		sql.append(" select count(*) as rowCount from user_bill where ");
		sql.append(" file_location=? and file_name=?");
		try {
			Connection connection = DataBaseConnectionUtil.getConnection();
			PreparedStatement preparedStatement = connection
					.prepareStatement(sql.toString());
			preparedStatement.setString(1, folderLocation);
			preparedStatement.setString(2, fileName);

			ResultSet rs = preparedStatement.executeQuery();
			int rowCount = 0;
			while (rs.next()) {
				rowCount = rs.getInt("rowCount");
			}
			rs.close();
			preparedStatement.close();
			connection.close();
			returnValue = (rowCount <= 0);
		} catch (SQLException sqlEx) {
			log.error(sqlEx);
			throw sqlEx;
		}
		return returnValue;
	}
}
