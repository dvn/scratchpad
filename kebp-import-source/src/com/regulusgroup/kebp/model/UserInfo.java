package com.regulusgroup.kebp.model;

public class UserInfo {
	private Long  id;
	

	private String uuid;
	private Long mrn;
	private String buid;
	private String email;
	private String mrnPrefix;
	private String regionId;
	private String dob;
	private String firstName;
	private String lastName;
	private Long userAccountId;
	private String paperOptOut;
	
	public Long getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Long userAccountId) {
		this.userAccountId = userAccountId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Long getMrn() {
		return mrn;
	}

	public void setMrn(Long mrn) {
		this.mrn = mrn;
	}

	public String getBuid() {
		return buid;
	}

	public void setBuid(String buid) {
		this.buid = buid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMrnPrefix() {
		return mrnPrefix;
	}

	public void setMrnPrefix(String mrnPrefix) {
		this.mrnPrefix = mrnPrefix;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	public String getPaperOptOut() {
		return paperOptOut;
	}

	public void setPaperOptOut(String paperOptOut) {
		this.paperOptOut = paperOptOut;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("[uuid=").append(uuid);
		sb.append("][mrn=").append(mrn);
		sb.append("][buid=").append(buid);
		sb.append("][email=").append(email);
		sb.append("][mrnPrefix=").append(mrnPrefix);
		sb.append("][regionId=").append(regionId);
		sb.append("][dob=").append(dob).append("]");
		sb.append("][firstName=").append(firstName).append("]");
		sb.append("][lastName=").append(lastName).append("]");
		return sb.toString();
	}
	
	public String getFullName(){
		return getFirstName()+" " + getLastName();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
