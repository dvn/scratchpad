package com.regulusgroup.kebp.tmp;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

public class CheckDuplicates {
	private static final Logger log = Logger.getLogger(CheckDuplicates.class);
	Map<String, String> map = new HashMap<String, String>(); 
	int duplicateCount=0;

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
	    FileFilter ctlFilter = new FileFilter() {
	        public boolean accept(File f) {
	            if (f.isDirectory()) {
	                return false;
	            }
	            String name = f.getName().toLowerCase();
	            return name.endsWith("ctl");
	        }
	    };

		File inbox = new File(args[0]);
        if (inbox.isDirectory()) {
            File[] children = inbox.listFiles(ctlFilter);
            if (children != null) {
            	for (int i = 0; i < children.length; i++) {
                    File ctlFile = children[i];
            		CheckDuplicates cd = new CheckDuplicates();
            		cd.processFile(ctlFile);
            	}
            }
        }
	}

	private void processFile(File file) throws IOException {
		log.info("****** Checking "+file.getAbsolutePath()+" for duplicates *******");
		DataInputStream dis = new DataInputStream(new FileInputStream(file));
		dis.readLine();
		int i =1;
		while(dis.available()!=-1){
			String s = dis.readLine();
			if(s==null || s.trim().length()==0){
				break;
			}
			String mrn = s.substring(858, 869);
			String mrn_prefix = s.substring(893, 896);
			String full_name = s.substring(904,945);
			String[] names = full_name.trim().split(",");
			String last_name = names[names.length-1];
			last_name = last_name.length()>5?last_name.substring(0, 5):last_name;
			addData(mrn+":"+mrn_prefix+":"+last_name,Integer.toString(i));
			i++;
		}
		dis.close();
		log.info("****** "+ duplicateCount +" duplicate records found ****** ");
	}
	
	private void addData(String key, String recordNum) {
		if(map.containsKey(key)){
			String value = map.get(key);
			log.info("Duplicate record "+key+" exists at record#"+value+"::"+recordNum);
			duplicateCount++;
		}else{
			map.put(key,recordNum);
		}
	}

}
