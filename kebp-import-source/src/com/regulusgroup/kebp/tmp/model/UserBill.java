package com.regulusgroup.kebp.tmp.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

import org.bson.types.ObjectId;

import com.google.code.morphia.Key;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@Entity("bill")
public class UserBill {
	@Id ObjectId id;	
    private String billNo;
    private Date billDate;
    private Date billDueDate;
    private BigDecimal amountBilled;
    private Key<UserAccount> account;
    private boolean pastDue;
    private byte[] image;
    private boolean available;
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public Date getBillDate() {
		return billDate;
	}
	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}
	public Date getBillDueDate() {
		return billDueDate;
	}
	public void setBillDueDate(Date billDueDate) {
		this.billDueDate = billDueDate;
	}
	public BigDecimal getAmountBilled() {
		return amountBilled;
	}
	public void setAmountBilled(BigDecimal amountBilled) {
		this.amountBilled = amountBilled;
	}
	public Key<UserAccount> getUserAccount() {
		return account;
	}
	public void setUserAccount(Key<UserAccount> userAccount) {
		this.account = userAccount;
	}
	public boolean isPastDue() {
		return pastDue;
	}
	public void setPastDue(boolean pastDue) {
		this.pastDue = pastDue;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	@Override
	public String toString() {
		return "UserBill [id=" + id + ", billNo=" + billNo + ", billDate="
				+ billDate + ", billDueDate=" + billDueDate + ", amountBilled="
				+ amountBilled + ", userAccount=" + account + ", pastDue="
				+ pastDue + ", image=" + new String(image)
				+ ", available=" + available + "]";
	}

}
