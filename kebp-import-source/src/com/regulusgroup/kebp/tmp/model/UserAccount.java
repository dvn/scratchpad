package com.regulusgroup.kebp.tmp.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;

import com.google.code.morphia.Key;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Reference;
import com.google.code.morphia.annotations.Transient;

@Entity("account")
public class UserAccount {
	@Id ObjectId id;	
	private String buid;
	private boolean paperOptOut;
	private Integer modifiedBy;
	private Date modifiedOn;
	private Key<UserInfo> user;
	@Reference("bill")
	private List<UserBill> userBillCollection;
	private String changedBy;
    private String accountType;
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getBuid() {
		return buid;
	}
	public void setBuid(String buid) {
		this.buid = buid;
	}
	public boolean isPaperOptOut() {
		return paperOptOut;
	}
	public void setPaperOptOut(boolean paperOptOut) {
		this.paperOptOut = paperOptOut;
	}
	public Integer getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public Key<UserInfo> getUserInfo() {
		return user;
	}
	public void setUserInfo(Key<UserInfo> userInfo) {
		this.user = userInfo;
	}
	public List<UserBill> getUserBillCollection() {
		return userBillCollection;
	}
	public void setUserBillCollection(List<UserBill> userBillCollection) {
		this.userBillCollection = userBillCollection;
	}
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	@Override
	public String toString() {
		return "UserAccount [id=" + id + ", buid=" + buid + ", paperOptOut="
				+ paperOptOut + ", modifiedBy=" + modifiedBy + ", modifiedOn="
				+ modifiedOn + ", userInfo=" + user
				+ ", userBillCollection=" + userBillCollection + ", changedBy="
				+ changedBy + ", accountType=" + accountType + "]";
	}

    
}
