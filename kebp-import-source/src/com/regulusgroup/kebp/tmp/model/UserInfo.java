package com.regulusgroup.kebp.tmp.model;

import java.util.List;

import org.bson.types.ObjectId;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Reference;
import com.google.code.morphia.annotations.Transient;

@Entity("user")
public class UserInfo {
	@Id ObjectId id;
    private String uuid;
    private String mrn;
    private String buid;
    private String email;
    private String firstName;
    private String lastName;
    private String dob;
    private String regionId;
    private String mrnPrefix;
    @Transient
    private String relayState;
    @Transient
    private String returnToKp;
    @Transient
    private String kporgdomain;
    @Transient
    private String efssodomain;
    @Transient
    private String errorUrl;
    
    @Reference("account")
    private List<UserAccount> userAccountCollection;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getMrn() {
		return mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public String getBuid() {
		return buid;
	}

	public void setBuid(String buid) {
		this.buid = buid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	public String getMrnPrefix() {
		return mrnPrefix;
	}

	public void setMrnPrefix(String mrnPrefix) {
		this.mrnPrefix = mrnPrefix;
	}

	public String getRelayState() {
		return relayState;
	}

	public void setRelayState(String relayState) {
		this.relayState = relayState;
	}

	public String getReturnToKp() {
		return returnToKp;
	}

	public void setReturnToKp(String returnToKp) {
		this.returnToKp = returnToKp;
	}

	public String getKporgdomain() {
		return kporgdomain;
	}

	public void setKporgdomain(String kporgdomain) {
		this.kporgdomain = kporgdomain;
	}

	public String getEfssodomain() {
		return efssodomain;
	}

	public void setEfssodomain(String efssodomain) {
		this.efssodomain = efssodomain;
	}

	public String getErrorUrl() {
		return errorUrl;
	}

	public void setErrorUrl(String errorUrl) {
		this.errorUrl = errorUrl;
	}

	public List<UserAccount> getUserAccountCollection() {
		return userAccountCollection;
	}

	public void setUserAccountCollection(List<UserAccount> userAccountCollection) {
		this.userAccountCollection = userAccountCollection;
	}

	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", uuid=" + uuid + ", mrn=" + mrn
				+ ", buid=" + buid + ", email=" + email + ", firstName="
				+ firstName + ", lastName=" + lastName + ", dob=" + dob
				+ ", regionId=" + regionId + ", mrnPrefix=" + mrnPrefix
				+ ", relayState=" + relayState + ", returnToKp=" + returnToKp
				+ ", kporgdomain=" + kporgdomain + ", efssodomain="
				+ efssodomain + ", errorUrl=" + errorUrl
				+ ", userAccountCollection=" + userAccountCollection + "]";
	}
	
    
}
