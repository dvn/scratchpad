package com.regulusgroup.kebp.tmp;

import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.logging.MorphiaLoggerFactory;
import com.google.code.morphia.logging.slf4j.SLF4JLogrImplFactory;
import com.google.code.morphia.query.UpdateResults;
import com.mongodb.DBRef;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.regulusgroup.kebp.tmp.model.UserAccount;
import com.regulusgroup.kebp.tmp.model.UserBill;
import com.regulusgroup.kebp.tmp.model.UserInfo;

public class MorphiaPersister {

	/**
	 * @param args
	 * @throws MongoException 
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException, MongoException {
		MorphiaLoggerFactory.registerLogger(SLF4JLogrImplFactory.class);	
		Datastore ds = new Morphia().createDatastore(new Mongo(), "mongofsm");
//		//UserInfo = 
//		UserAccount account = ds.find(UserAccount.class).filter("userInfo", ds.getKey(ds.get(UserInfo.class, new ObjectId("4f19dc991c638285ec247663")))).get();
//		System.out.println(account);

		/*
		long t1 = System.currentTimeMillis();
		ObjectId id = new ObjectId("4f19cdac1c63702b78cab6a5");
		UserInfo u = ds.get(UserInfo.class, id);
		long t2 = System.currentTimeMillis();
		System.out.println((t2-t1)+" ms:"+u);
		
		long t3 = System.currentTimeMillis();
		ObjectId id1 = new ObjectId("4f19c5721c63afaaf048c3bd");
		UserAccount ua = ds.get(UserAccount.class, id1);
		long t4 = System.currentTimeMillis();
		System.out.println((t4-t3)+" ms:"+ua);
		
		long t5 = System.currentTimeMillis();
		ObjectId id2 = new ObjectId("4f19c5721c63afaaf048c3bc");
		UserBill ub = ds.get(UserBill.class, id2);
		long t6 = System.currentTimeMillis();
		System.out.println((t6-t5)+" ms:"+ub);		

		UserBill bill = new UserBill();
		bill.setBillDate(new Date("12/12/2010"));
		bill.setAmountBilled(new BigDecimal("12.12"));
		bill.setImage("testimage2".getBytes());
		ds.save(bill);
		
		ua.getUserBillCollection().add(bill);
		ds.save(ua);
		*/
		//DBRef 
		
		
		UserInfo user = new UserInfo();
		user.setMrn("M8888");
		user.setDob("10/1975");
		Key<UserInfo> userkey = ds.save(user);

		UserAccount account = new UserAccount();
		account.setAccountType("electric");
		account.setBuid("B99999");
		account.setUserInfo(userkey);
		Key<UserAccount> accountKey = ds.save(account);
		
		UpdateResults<UserInfo> res = ds.update(user, ds.createUpdateOperations(UserInfo.class).add("account", accountKey));
		
		UserBill bill = new UserBill();
		bill.setBillDate(new Date());
		bill.setImage("testimage111".getBytes());
		bill.setUserAccount(accountKey);
		Key<UserBill> billKey = ds.save(bill);
		UpdateResults<UserAccount> res1 = ds.update(account, ds.createUpdateOperations(UserAccount.class).add("bill", billKey));
		
		
//		List<UserBill> bills = new ArrayList<UserBill>();
//		bills.add(bill);
//		
//		
//		List<UserAccount> accounts = new ArrayList<UserAccount>();
//		accounts.add(account);
		
		
	}

}
