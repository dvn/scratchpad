package com.regulusgroup.kebp.tmp;

import java.net.UnknownHostException;

import org.bson.types.ObjectId;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Key;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.logging.MorphiaLoggerFactory;
import com.google.code.morphia.logging.slf4j.SLF4JLogrImplFactory;
import com.google.code.morphia.query.Query;
import com.google.code.morphia.query.UpdateResults;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.regulusgroup.kebp.tmp.model.UserAccount;
import com.regulusgroup.kebp.tmp.model.UserInfo;

public class MorphiaAddAccount {

	public static void main(String[] args) throws UnknownHostException, MongoException {
		MorphiaLoggerFactory.registerLogger(SLF4JLogrImplFactory.class);	
		Datastore ds = new Morphia().createDatastore(new Mongo(), "mongofsm");
		UserInfo ua = ds.get(UserInfo.class, new ObjectId("4f19e1911c63694043df4d46"));
		System.out.println(ua);
		
		UserAccount user = ds.find(UserAccount.class).filter("user", ds.getKey(ua)).get();
		if(user==null){
			System.out.println("*** NULL ***");
		}else{
			System.out.println("NOT NULL:"+user);
			System.out.println("NOT NULL:"+ds.get(UserInfo.class, user.getUserInfo().getId()));
		}
		
//		for(UserAccount ua : user.getUserAccountCollection()){
//			System.out.println(ua);
//		}
		
//		UserAccount account = ds.get(UserAccount.class, new ObjectId("4f19e1911c63694043df4d47"));
//		ds.delete(account);
		
//		UserAccount account = new UserAccount();
//		account.setAccountType("water");
//		account.setBuid("B22222");
//		account.setUserInfo(ds.getKey(user));
//		Key<UserAccount> accountKey = ds.save(account);
//		
//		UpdateResults<UserInfo> res = ds.update(user, ds.createUpdateOperations(UserInfo.class).add("account", accountKey));
		
	}

}
