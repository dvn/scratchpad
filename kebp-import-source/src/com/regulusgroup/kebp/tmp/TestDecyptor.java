package com.regulusgroup.kebp.tmp;

import com.regulusgroup.kebp.Crypto;
import com.regulusgroup.kebp.FileSystemManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;

public class TestDecyptor {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws DecoderException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 */
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, DecoderException, IOException, IllegalBlockSizeException, BadPaddingException {
		// TODO Auto-generated method stub
		
//		long t0 = System.currentTimeMillis();
//		Crypto crypto = new Crypto();
//		FileSystemManager fsm = new FileSystemManager();
//
//		String path1 = "E:\\kebptest\\splitter\\temp\\testing1.txt";
//		File file = new File(path1);
//		boolean flag = fsm.secureDelete(file);
//		System.out.println("secure delete:"+flag);
//
//		String path2 = "E:\\kebptest\\splitter\\temp\\testing2.txt";
//		boolean flag2 = fsm.secureDelete(path2);
//		System.out.println("secure delete 2:"+flag2);

//		String path = fsm.getSavePath("E:\\kebptest\\splitter\\temp\\20100112-000000001-944320304.pdf");
//        fsm.secureDelete("E:\\kebptest\\splitter\\temp\\20100112-000000001-944320304.pdf");               // Larger file size will slow down secure delete also.
//		
//		System.out.println("Path:"+path);
//		
//		long t1 = System.currentTimeMillis();
////		fsm.decryptAndRead("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf", "E:/kebptest/splitter/outbox/7fcab1118a68385a712f.pdf");
//		long t2 = System.currentTimeMillis();
//		byte[] buffer = crypto.decrypt(new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf"));
//		long t3 = System.currentTimeMillis();
//		byte[] buffer1 = crypto.decrypt(new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf"));
//		byte[] buffer2 = crypto.decrypt(new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf"));
//		byte[] buffer3 = crypto.decrypt(new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf"));
//		byte[] buffer4 = crypto.decrypt(new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf"));
//		byte[] buffer5 = crypto.decrypt(new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf"));
//		long t4 = System.currentTimeMillis();
//		
////        FileInputStream in = new FileInputStream("E:/FSM/201002/0b6/e10/e09/f76/7fcab1118a68385a712f.pdf");
////        byte[] b1 = new byte[in.available()];
////        in.read(b1);
////        in.close();
//		long t5 = System.currentTimeMillis();
//		
//        System.out.println("fsm Initialize"+(t1-t0));
//        System.out.println("fsm.decryptAndRead Write"+(t2-t1));
//        System.out.println("fsm.decryptAndRead Bytes 1:"+(t3-t2));
//        System.out.println("fsm.decryptAndRead Bytes 2:"+(t4-t3));
//        System.out.println("Read Bytes from file"+(t5-t4));
//        System.out.println("Total Time"+(t5-t0));
	}

}
