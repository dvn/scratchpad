/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.regulusgroup.kebp.tmp;

import java.security.spec.*;
import java.security.*;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.security.MessageDigest; // import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.DecoderException; // import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec; // import javax.crypto.SecretKey;
// import javax.crypto.SecretKeyFactory;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.BadPaddingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;

/**
 * 
 * @author kaushika
 */
public class Crypto {

	private final String KEY_ALGORITHM = "AES";
	private final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
	//private final String PROPS = "pdfutil.properties";
	private final String CHARSET_NAME = "UTF-8";
	private final String DIGEST_ALGORITHM = "MD5";
	private String KeyfilePath = "";
	private String PassPhraseStr = "";
	private String PassPhraseMD5 = "";
	private Cipher encrypter = null;
	private Cipher decrypter = null;
	private InputStream in_global = null;
	private OutputStream out_global = null;
	private byte[] iv = null;
	private final byte[] key = new byte[16];

	Crypto() throws DecoderException, InvalidKeyException, IOException,
			NoSuchAlgorithmException, InvalidKeySpecException,
			NoSuchPaddingException, InvalidAlgorithmParameterException {
		// Large constructor - keep this object around and reuse.
		this.loadconfig();

		PassPhraseMD5 = getMD5(PassPhraseStr);
		byte[] keyBytes = PassPhraseMD5.getBytes(CHARSET_NAME);
		System.arraycopy(keyBytes, 0, key, 0, 16);

		/* Initialization Vector of 8 bytes */
		iv = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
				0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };

		/* Initialize the encrypter class */
		encrypter = Cipher.getInstance(CIPHER_ALGORITHM);
		encrypter.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key,
				KEY_ALGORITHM), new IvParameterSpec(iv));

		/* Initiliaze the decrypter class */
		decrypter = Cipher.getInstance(CIPHER_ALGORITHM);
		decrypter.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key,
				KEY_ALGORITHM), new IvParameterSpec(iv));

		java.util.Arrays.fill(key, (byte) 0x00);
		java.util.Arrays.fill(iv, (byte) 0x00);

	}

	private void loadconfig() throws IOException {

		//Properties props = new Properties();

		/* Load properties file */
		//props.load(new BufferedInputStream(new FileInputStream(PROPS)));

		/* Get the path to the Keyfile */
		//KeyfilePath = props.getProperty("KeyfilePath");

		/* Get the text stored in the Keyfile */
		//File KeyFile = new File(KeyfilePath);
		//props.load(new BufferedInputStream(new FileInputStream(KeyFile)));
		PassPhraseStr = "loadtesting passphrase";

	}

	private String getMD5(String str) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {
		MessageDigest md5 = null;
		byte[] hash = null;
		StringBuffer sb = new StringBuffer();
		md5 = MessageDigest.getInstance(DIGEST_ALGORITHM);
		md5.update(str.getBytes(CHARSET_NAME));
		hash = md5.digest();
		for (int i = 0; i < hash.length; i++) {
			String tmpStr = "0" + Integer.toHexString((0xff & hash[i]));
			sb.append(tmpStr.substring(tmpStr.length() - 2));
		}
		// System.out.println(sb.toString());
		return sb.toString();
	}

	/**
	 * Use the specified TripleDES key to encrypt bytes from the input stream
	 * and write them to the output stream. This method uses CipherOutputStream
	 * to perform the encryption and write bytes at the same time.
	 **/
	public boolean encrypt(InputStream in, OutputStream out)
			throws NoSuchAlgorithmException, InvalidKeyException,
			NoSuchPaddingException, IOException {
		boolean success = false;

		/* Create a special output stream to do the work for us */
		CipherOutputStream cos = new CipherOutputStream(out, encrypter);

		/* Read from the input and write to the encrypting output stream */
		byte[] buffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = in.read(buffer)) != -1) {
			cos.write(buffer, 0, bytesRead);
		}
		cos.flush();
		cos.close();
		out.close();
		in.close();
		/* For extra security, don't leave any plaintext hanging around memory. */
		java.util.Arrays.fill(buffer, (byte) 0x00);
		success = true;

		return success;
	}

	/**
	 * Use the specified TripleDES key to decrypt bytes ready from the input
	 * stream and write them to the output stream. This method uses uses Cipher
	 * directly to show how it can be done without CipherInputStream and
	 * CipherOutputStream.
	 **/
	public boolean decrypt(InputStream in, OutputStream out)
			throws NoSuchAlgorithmException, InvalidKeyException, IOException,
			IllegalBlockSizeException, NoSuchPaddingException,
			BadPaddingException {

		boolean success = false;

		/* Bytes read from in will be decrypted */
		CipherInputStream cis = new CipherInputStream(in, decrypter);

		/* Read in the decrypted bytes and write the cleartext to out */
		byte[] buffer = new byte[2048];
		int bytesRead;
		while ((bytesRead = cis.read(buffer)) != -1) {
			// System.out.println(String.valueOf(bytesRead));
			out.write(buffer, 0, bytesRead);
		}
		out.close();

		/* For extra security, don't leave any plaintext hanging around memory. */
		java.util.Arrays.fill(buffer, (byte) 0x00);
		success = true;

		return success;
	}

	/**
	 * Use the specified TripleDES key to decrypt bytes ready from the input
	 * stream and return as bye array. This method uses uses Cipher
	 * directly to show how it can be done without CipherInputStream and
	 * CipherOutputStream.
	 **/
	public byte[] decrypt(InputStream in)
			throws NoSuchAlgorithmException, InvalidKeyException, IOException,
			IllegalBlockSizeException, NoSuchPaddingException,
			BadPaddingException {

		/* Bytes read from in will be decrypted */
		CipherInputStream cis = new CipherInputStream(in, decrypter);
		/* Read in the decrypted bytes and write the cleartext to out */

		byte[] buffer = new byte[2048];
		byte[] outbytes = new byte[in.available()];
		int bytesRead;
		int position = 0;
		while ((bytesRead = cis.read(buffer)) != -1) {
	        System.arraycopy(buffer, 0, outbytes, position, bytesRead);
	        position += bytesRead;
		}
		
		/* For extra security, don't leave any plaintext hanging around memory. */
		java.util.Arrays.fill(buffer, (byte) 0x00);
		cis.close();
		in.close();
		return outbytes;
	}

	public byte[] getKey() {
		return key;
	}
	
	public static void main(String[] args) throws Exception{
		InputStream fis = new FileInputStream("D:/Users/31431/Documents/ebp-platform/pge/bpp.pdf");
		OutputStream fos = new BufferedOutputStream(new FileOutputStream("D:/Users/31431/Documents/ebp-platform/pge/253dd517d2ad9b5b8373.pdf"));
		Crypto crypto = new Crypto();
		crypto.encrypt(fis, fos);
		fos.close();
		fis.close();
	}
}
