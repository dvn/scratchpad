package com.regulusgroup.kebp.tmp;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.regulusgroup.kebp.RecordReference;

public class MongoPersister {
	static Mongo mongo;
	static DB db;
	static DBCollection mfs;

	public MongoPersister(){
		try {
			mongo = new Mongo();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
		db = mongo.getDB("mongofsm");
		mfs = db.getCollection("filestorage");
	}

	public static void save(RecordReference recRef, byte[] data){
		if(mongo==null){
			try {
				mongo = new Mongo();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (MongoException e) {
				e.printStackTrace();
			}
			db = mongo.getDB("mongofsm");
			mfs = db.getCollection("filestorage");
			
		}
		//saveRecordReference(recRef);

		BasicDBObject file = new BasicDBObject();
		file.put("id", recRef.getSave_path());
		file.put("image", data);
		mfs.insert(file);
	}
	
	/*
	public static boolean saveRecordReference(RecordReference recordReference)
			throws Exception {
		// System.out.println(" Inside saveRecordReference");
		boolean isBillSaved = false;
		try {
			HashMap tokens = recordReference.getRowTokens();
			String mrnNumber = (String) tokens.get("MRN_NUM");
			String mrnPrefix = (String) tokens.get("MRN_PREFIX");
			String fullName = (String) tokens.get("FULL_NAME");
			String pastDue = (String) tokens.get("PAST_DUE");
			String billDate = new SimpleDateFormat("yyyy-mm-dd")
					.format(new SimpleDateFormat("mm/dd/yyyy")
							.parse((String) tokens.get("BILL_DATE")));
			String buId = recordReference.getAcct_num();
			String[] nameArr = fullName.trim().split(nameDelimeter);
			String firstName = "";
			for (int i = 0; i <= nameArr.length - 2; i++) {
				if (i == 0) {
					firstName = nameArr[i];
				} else {
					firstName = firstName + " " + nameArr[i];
				}
			}
			String lastName = nameArr[nameArr.length - 1];
			log.trace("Fullname:" + fullName);

			if (lastName!=null && lastName.trim().equals("")) {
				log.error("Last Name is blank... Ignoring record...:"
						+ recordReference.eid_key + " " + fullName + ", "
						+ recordReference.getSave_path());
				return false;
			}

			if (!firstName.matches("^[a-z A-Z-.']+$")
					|| !lastName.matches("^[a-z A-Z-.']+$")) {
				log
						.error("Name contains special characters... Ignoring record...:"
								+ recordReference.eid_key
								+ " "
								+ fullName
								+ ", " + recordReference.getSave_path());
				return false;
			}
			String partialLastName = lastName.length() > LAST_NAME_SEARCH_LENGTH ? lastName
					.substring(0, LAST_NAME_SEARCH_LENGTH)
					: lastName;
			// Replace ' in the name with mysql escape sequence
			// firstName = firstName.replace("'", "\\'");
			// lastName = lastName.replace("'", "\\'");
			// partialLastName = partialLastName.replace("'", "\\'");
			// Get the existing userInfo Id for the
			// buid and mrn combination
			Integer userInfoId = getUserInfoId(mrnNumber, mrnPrefix,
					partialLastName);
			Integer userAccountId = null;
			if (userInfoId == null) {
				saveUserInfo(mrnNumber, mrnPrefix, firstName, lastName);
			} else {
				// update the existing first name with the new first name
				updateUserInfo(firstName, userInfoId);
			}
			boolean mailSentUpdated = updateMailStatus(mrnNumber, mrnPrefix,
					partialLastName, 0);
			userAccountId = getUserAccountId(userInfoId, buId);
			if (userAccountId == null) {
				saveUserAccount(mrnNumber, mrnPrefix, partialLastName, buId);
			}
			isBillSaved = saveOrUpdateUserBill(mrnNumber, mrnPrefix,
					partialLastName, recordReference, buId, pastDue, billDate);
		} catch (Exception e) {
			log.error(e);
			throw e;
		}
		return isBillSaved;

	}
	*/
	public static void close(){
		mongo.close();
	}
}
