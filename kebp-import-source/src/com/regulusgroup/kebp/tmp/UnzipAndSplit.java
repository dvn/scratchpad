package com.regulusgroup.kebp.tmp;

import org.apache.log4j.Logger;

import com.regulusgroup.kebp.SplitCtlFiles;
import com.regulusgroup.kebp.UnzipAllFiles;


public class UnzipAndSplit {
	private static final Logger logger = Logger.getLogger(UnzipAndSplit.class);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new UnzipAllFiles().unzipFiles();
		new SplitCtlFiles().splitCtlFiles();

	}

}
