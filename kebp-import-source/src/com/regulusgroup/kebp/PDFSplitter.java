/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.regulusgroup.kebp;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.SimpleBookmark;

/**
 *
 * @author kaushika
 */
public class PDFSplitter {

    private SimpleLog log = new SimpleLog();
    private String sep = File.separator;
    private Crypto crypto = null;
    private File CutFile = null;
    private String CutFileName = null;
    private String CutFileAbsPath = null;
    private PdfReader reader = null;
    private SortedMap<Integer, BookmarkObject> BookmarkHM = null;
    private String sourceFile = null;
    private String destinationFile = null;

    public PDFSplitter() throws DecoderException, InvalidKeyException, IOException,
            NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException,
            InvalidAlgorithmParameterException {
        // Initialize the Crypto provider.
        crypto = new Crypto();
    }

    public void loadSourceFile(String name_of_file_to_be_split) {
        // Need to load all lines in txt file
        CutFileName = name_of_file_to_be_split;
        try {
            CutFile = new File(CutFileName);
            CutFileAbsPath = CutFile.getAbsolutePath();
            // System.out.println("CutFile [" + CutFileAbsPath +"]");
            // Load the file to be read.
            reader = new PdfReader(new PdfReader(name_of_file_to_be_split));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    public void closeSourceFile () {
        if(!(reader == null)) {
            reader.close();
        }
    }

    public void reloadSourceFile () {
        if(! (reader == null) ) {
            closeSourceFile();
        }
        loadSourceFile(CutFileAbsPath);
    }

    public void readBookmarks() {
        // Load the Bookmarks in the file.
        List list = SimpleBookmark.getBookmark(reader);

        BookmarkHM = new TreeMap<Integer, BookmarkObject>();
        String prevTitle = "";
        int prevStart = 0;
        int prevEnd = 0;
        String key = "";
        for (Iterator iterator = list.iterator(); iterator.hasNext();) {
            Map<String, Object> object = (Map) iterator.next();
            String currTitle;
            int currStart = 0;
            currTitle = (String) object.get("Title");
            currStart = Integer.parseInt(((String) object.get("Page")).split(" ")[0]);
            prevEnd = currStart - 1;

            if (prevStart != 0) {
                // Overwrites earlier keys.
                BookmarkHM.put(new Integer(key), new BookmarkObject(prevTitle, prevStart, prevEnd));
            }

            prevTitle = currTitle;
            key = currTitle.split(":")[0];
            prevStart = currStart;
            if (!iterator.hasNext()) {
                prevEnd = reader.getNumberOfPages();
            }
        }
        // Overwrites earlier keys
        BookmarkHM.put(new Integer(key),new BookmarkObject(prevTitle, prevStart, prevEnd));
        // End of reading bookmarks
    }

    public void printBookmarkHash() {
        java.util.Set set = BookmarkHM.entrySet();
        java.util.Iterator i = set.iterator();
        while ( i.hasNext() ) {
            Map.Entry me = (Map.Entry) i.next();
            System.out.println("Key ["+me.getKey() + "]: Value[" + ((BookmarkObject)me.getValue()).toString()+ "]");
        }
    }

    public void printBookmark(String key ) {
        BookmarkObject boj = (BookmarkObject)BookmarkHM.get(key);
        if ( boj == null) {
            System.out.println("KEY ["+key+"] does not exist");
        } else {
            System.out.println("KEY ["+key+"] Value["+boj.toString()+"]");
        }
    }

    public byte[] extractPgsUsingPageIndex(int startindex, int stopindex)
            throws IOException {
        int numpgs = 0;
        ByteArrayOutputStream baos = null;
        try {
            // Step 1 - create a document object.
            Document document = new Document(reader.getPageSizeWithRotation(1));
            baos = new ByteArrayOutputStream();
            BufferedOutputStream bos = new BufferedOutputStream(baos);
            
            // Step 2: we create a writer that listens to the document
            PdfCopy writer = new PdfCopy(document, bos);

            // Step 3: Open document for write.
            document.open();

            // Note that startindex and stopindex get set for every line in cut file.
            // Don't like using this blunt (and silent) amendment.
            numpgs = reader.getNumberOfPages();
            if (startindex < 0) {startindex = 0;}
            if (stopindex > numpgs) {stopindex = numpgs;}

            for (int i = startindex; i <= stopindex; i++) {
                PdfImportedPage page = writer.getImportedPage(reader, i);
                writer.addPage(page);
            }
            // Step 5: Close the document
            document.close();
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

    public byte[] extractPgsUsingBookmarks(String Bookmark) throws IOException {
    	byte[] extractedPage = null;
        try {
            // Lookup the bookmark.
            BookmarkObject bobject = (BookmarkObject)BookmarkHM.get(Bookmark);
            int startindex = bobject.pageFrom;
            int stopindex  = bobject.pageTo;
            
            // Use the indexes to extract the pages.
            extractedPage = extractPgsUsingPageIndex(startindex, stopindex);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return extractedPage;
    }

    public int getBookmarkStartIndex(String Bookmark)
    {
        BookmarkObject bobject = (BookmarkObject)BookmarkHM.get(Bookmark);
        int startindex = bobject.pageFrom;
        return startindex;
    }

    public int getBookmarkStopIndex(String Bookmark)
    {
        BookmarkObject bobject = (BookmarkObject)BookmarkHM.get(Bookmark);
        int stopindex = bobject.pageTo;
        return stopindex;
    }

    /**
     * Returns the current memory use.
     *
     * @return the current memory use
     */
    public long getMemoryUse() {
        garbageCollect();
        garbageCollect();
        garbageCollect();
        garbageCollect();
        long totalMemory = Runtime.getRuntime().totalMemory();
        garbageCollect();
        garbageCollect();
        long freeMemory = Runtime.getRuntime().freeMemory();
        return (totalMemory - freeMemory);
    }

    /**
     * Makes sure all garbage is cleared from the memory.
     */
    public void garbageCollect() {
        try {
            System.gc();
            Thread.sleep(100);
            System.runFinalization();
            Thread.sleep(100);
            System.gc();
            Thread.sleep(100);
            System.runFinalization();
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public PdfReader getReader(){
    	return reader;
    }

    public SortedMap<Integer, BookmarkObject> getBookmarkHM(){
    	return BookmarkHM;
    }

    public BookmarkObject getBookmarkHMValues(Integer key){
    	return BookmarkHM.get(key);
    }
    
    class BookmarkObject {

        String title;
        String buid;
        int pageFrom;
        int pageTo;

        public BookmarkObject(String title, int pageFrom, int pageTo) {
            this.title = title;
            this.buid = title.split(" ")[1];
            this.pageFrom = pageFrom;
            this.pageTo = pageTo;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
            this.buid = title.split(" ")[1];
        }

        public String getBuid() {
            return buid;
        }

        public void setBuid(String buid) {
            this.buid = buid;
        }

        public int getPageFrom() {
            return pageFrom;
        }

        public void setPageFrom(int pageFrom) {
            this.pageFrom = pageFrom;
        }

        public int getPageTo() {
            return pageTo;
        }

        public void setPageTo(int pageTo) {
            this.pageTo = pageTo;
        }

        public String toString() {
            return "TOCObj[title=" + title + ",buid=" + buid + ",pageFrom=" + pageFrom + ",pageTo=" + pageTo + "]";
        }
    }
}
