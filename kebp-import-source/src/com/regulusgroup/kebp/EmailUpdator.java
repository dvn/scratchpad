package com.regulusgroup.kebp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.csvreader.CsvReader;
import com.regulusgroup.kebp.model.UserInfo;

/**
 * 
 * @author Ajay Krishnamurthy
 * 
 */

public class EmailUpdator {
	private static File csvInbox = null;
	private static Properties props = null;
	private static String mrnPrefixHeader;
	private static String mrnHeader;
	private static String buidHeader;
	private static String emailHeader;
	private static String regionIdHeader;
	private static String dobHeader;
	private static String lastNameHeader;

	// This filter only the .csv files
	private static FileFilter csvFilter = new FileFilter() {
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return false;
			}
			String name = f.getName().toLowerCase();
			return name.endsWith("csv");
		}
	};

	private static void loadconfig() {
		// Look at the props file and find the inbox
		props = new Properties();
		try {
			props.load(new BufferedInputStream(new FileInputStream(
					"email_updator.properties")));
			csvInbox = new File(props.getProperty("InboxDir"));
			mrnPrefixHeader = props.getProperty("MrnPrefixHeader");
			mrnHeader = props.getProperty("MrnHeader");
			buidHeader = props.getProperty("BuidHeader");
			emailHeader = props.getProperty("EmailHeader");
			regionIdHeader = props.getProperty("RegionIdHeader");
			dobHeader = props.getProperty("DOBHeader");
			lastNameHeader = props.getProperty("LastNameHeader");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException, SQLException {
		updateEmails();
	}

	public static boolean updateEmails() throws IOException, SQLException {
		// Load the Configuration Details
		loadconfig();
		if (csvInbox.isDirectory()) {
			File[] children = csvInbox.listFiles(csvFilter);
			if (children == null) {
				// there is no csv files
				System.out.println("There is no csv Files inside the folder");
			} else {
				DataPersister dataPersister = new DataPersister();
				for (int i = 0; i < children.length; i++) {
					File csvFile = children[i];
					// Read the File each Line and generate the
					List<UserInfo> userInfoList = new ArrayList<UserInfo>();
					CsvReader reader;
					try {
						reader = new CsvReader(new FileReader(csvFile));
						// Read the Headers
						reader.readHeaders();
						while (reader.readRecord()) {
							String strMrn = reader.get(mrnHeader);
							UserInfo userInfo = new UserInfo();
							userInfo.setMrnPrefix(reader.get(mrnPrefixHeader));
							if (strMrn != null && !strMrn.equals("")) {
								userInfo.setMrn(Long.valueOf(strMrn));
							}
							userInfo.setBuid(reader.get(buidHeader));
							userInfo.setEmail(reader.get(emailHeader));
							userInfo.setLastName(reader.get(lastNameHeader));
							/*
							 * userInfo.setRegionId(reader.get(regionIdHeader));
							 * userInfo.setDob(reader.get(dobHeader));
							 */
							userInfoList.add(userInfo);
						}
						reader.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// Save the userInfo Objects
					if (userInfoList.size() > 0) {
						dataPersister.updateUserInfoEmails(userInfoList);
						System.out.println("Users emails have been successfully updated");
					}
				}

			}

		} else {
			System.out.println("The Inox [" + csvInbox.getAbsolutePath()
					+ "] is not poining to a valid Directory");
		}
		return true;
	}
}
