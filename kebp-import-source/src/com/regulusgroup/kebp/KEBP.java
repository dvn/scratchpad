/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.regulusgroup.kebp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.regulusgroup.kebp.PDFSplitter.BookmarkObject;
import com.regulusgroup.kebp.tmp.MongoPersister;

/**
 * 
 * @author kaushika
 */
public class KEBP {

	private static PDFSplitter pdfs = null;
	private static PDFConcatenator pdfConcatenator = null;
	private static PDFWatermark pdfWatermark = null;
	private static FWParser fwp = null;
	private static FileSystemManager fsm = null;
	private static Properties props = null;
	private static File inbox = null;
	private static File outbox = null;
	private static File tempdir = null;
	private static final String sep = File.separator;
	private static HashMap HeaderTokens = null;
	private static int CurDataRow = 0;
	private static int MaxDataRow = -1;
	private static int StartDataRow = 0;
	private static String PS2PDF_cmd = null;
	private static String PS2PDF_ext = null;
	private static String allLanguageBacker = null;
	private static String englishBacker = null;
	private static String englishAndSpanishBacker = null;
	private static String englishAndChineseBacker = null;
	static long appStartTime = System.currentTimeMillis();
	private static FileInputStream fis = null;
	private static byte[] allLanguageBackerBytes = null;
	private static byte[] englishBackerBytes = null;
	private static byte[] englishAndSpanishBackerBytes = null;
	private static byte[] englishAndChineseBackerBytes = null;
	private static byte[] billBytes;
	private static int gc_collect_rows;
	// private static int batch_update_max_rows;
	private static String ctlFileName = null;
	// This filter only returns CTL files.
	private static boolean saveToFsm = true;
	private static boolean saveToDb = true;
	private static boolean encryption = true;
	private static boolean unzipFiles = true;
	private static String monthsep = "";
	private static String processeidkey = null;
	private static boolean processSkippedRecords = false;
	private static boolean processOnlyAllowedSpecChars = false;
	private static boolean processOnlyMorethanOne = false;
	private static boolean findMissedRecords = false;
	private static final Logger log = Logger.getLogger(KEBP.class);
	private static FileFilter ctlFilter = new FileFilter() {

		public boolean accept(File f) {
			if (f.isDirectory()) {
				return false;
			}
			String name = f.getName().toLowerCase();
			return name.endsWith("ctl");
		}
	};
	private static FilenameFilter dirFilter = new FilenameFilter() {

		public boolean accept(File dir, String name) {
			return !name.startsWith(".");
		}
	};


	public static void main(String... arguments) throws Exception {
		// get a single ctl file from the argument
		if (arguments.length > 0) {
			for (String str : arguments) {
				if (str.equals("-fsm")) {
					saveToFsm = false;
				} else if (str.equals("-db")) {
					saveToDb = false;
				} else if (str.equals("-encrypt")) {
					encryption = false;
				} else if (str.equals("-unzip")) {
					unzipFiles = false;
				} else if (str.startsWith("-ctl")) {
					ctlFileName = str.split("=")[1];
				} else if (str.startsWith("-msep")) {
					monthsep = str.split("=")[1];
				} else if (str.startsWith("-eidkey")) {
					processeidkey = str.split("=")[1];
				} else if (str.startsWith("-psr")) {
					String paramString = str.split("=")[1];
					String[] paramArray = paramString.split(",");
					processSkippedRecords = paramArray[0].equals("true") ? true
							: false;
					if (paramArray.length > 1) {
						String logicString = paramArray[1];
						if (logicString.equalsIgnoreCase("both")) {
							processOnlyAllowedSpecChars = true;
							processOnlyMorethanOne = true;
						} else if (logicString
								.equalsIgnoreCase("onlyAllowedSpecChars")) {
							processOnlyAllowedSpecChars = true;
						} else if (logicString
								.equalsIgnoreCase("onlyMorethanOne")) {
							processOnlyMorethanOne = true;
						}
					}
				} else if (str.startsWith("-fmr")) {
					findMissedRecords = str.split("=")[1].equals("true")?true:false;
				}
			}
		}
		if (unzipFiles) {
			log.trace("Import process Started...");
			log.debug("Unzipping files...");
			UnzipAllFiles unzipAllFiles = new UnzipAllFiles();
			unzipAllFiles.unzipFiles();
			log.trace("Unzipping files done...");
			log.debug("Splitting control files into 15000 bills each...");
			SplitCtlFiles splitCtlFiles = new SplitCtlFiles();
			splitCtlFiles.splitCtlFiles();
			log.trace("Splitting control files done...");
		}
		long t1 = System.currentTimeMillis();
		try {
			log.trace("Initializing Fwparser...");
			fwp = new FWParser("fwp.properties");
			log.trace("Initializing PDFSplitter...");
			pdfs = new PDFSplitter();
			log.trace("Initializing PDFWatermark...");
			pdfWatermark = new PDFWatermark();
			log.trace("Initializing FileSystemManager...");
			fsm = new FileSystemManager();
			log.trace("Initializing PDFContatenator...");
			pdfConcatenator = new PDFConcatenator();
		} catch (Exception e) {
			log.fatal(e.getStackTrace());
		}

		// Load configuration file.
		log.trace("Loading config...");
		loadconfig();

		// Pick up the CTL files from inbox, and process corresponding .pdf file
		log.trace("Picking up the CTL files from inbox...");
		if (inbox.isDirectory()
				|| (ctlFileName != null && !ctlFileName.equals(""))) {

			File[] children = null;
			if (ctlFileName != null && !ctlFileName.equals("")) {
				children = new File[] { new File(ctlFileName) };
			} else {
				children = inbox.listFiles(ctlFilter);
			}
			if (children == null) {
				// No ctl files in dir. exit (and get launched again via cron
				// later.
				if (ctlFileName != null && !ctlFileName.equals("")) {
					log.error("Invalid CTL File [" + ctlFileName + "]");
				} else {
					log.error("No ctl files found in Inbox ["
							+ inbox.getAbsolutePath() + "]");
				}
				System.exit(0);
			} else {
				log.trace(children.length + " CTL files found...");
				// Process all the ctl files.
				for (int i = 0; i < children.length; i++) {
					// Get filename of file or directory
					File ctlFile = children[i];
					log.debug("Processing ctl file "
							+ ctlFile.getAbsolutePath());
					// Get the name of the PDF file from the CTL File header
					// row.
					// Make sure that you get the header row and then reload the
					// file.
					fwp.closeFile();
					log.trace("loading ctl file " + ctlFile.getAbsolutePath());
					fwp.loadFile(ctlFile.getAbsolutePath(), 0);
					log.trace("Reading header line from control file "
							+ ctlFile.getAbsolutePath());
					HeaderTokens = fwp.readHeaderLine();
					fwp.closeFile();

					// Get PDF name from token, get path from inbox
					// String pdfFileName = ((String)
					// HeaderTokens.get("FILE_NAME")).trim(); // Case Sensitive
					String pdfFileName = ctlFile.getName().substring(0,
							ctlFile.getName().lastIndexOf((".ctl")))
							+ ".pdf";
					// relative path file
					// name
					// fwp.printTokens(tokens);
					System.out.println("PATH::" + inbox.getAbsolutePath() + sep
							+ pdfFileName);
					File pdfFile = new File(inbox.getAbsolutePath() + sep
							+ pdfFileName);
					log.debug("Checking pdf file if exists "
							+ pdfFile.getAbsolutePath());

					if (pdfFile.exists()) {
						// Process the pdf and ctl file pair
						log.debug("Processing ctl file "
								+ ctlFile.getAbsolutePath() + " & pdf file "
								+ pdfFile.getAbsolutePath());
						process_file(pdfFile, ctlFile);
						// Print Error
					} else {
						log.error("CTL[" + ctlFile.getAbsolutePath()
								+ "] header points to non-existent PDF ["
								+ pdfFile.getAbsoluteFile() + "]");
						continue;
					}
					log.info("File: " + ctlFile.getAbsolutePath()
							+ " processed successfully...");
					fwp.closeFile();
					// File f = new File(ctlFile.getAbsolutePath());
					ctlFile.delete();
					pdfFile.delete();
				}
				// System.exit(0);
			}
		} else {
			// Error
			log.error("Inbox [" + inbox.getAbsolutePath()
					+ "] does not point to a valid directory");
		}
		long t2 = System.currentTimeMillis();
		System.out.println("********************** Time Taken: "+(t2-t1)+" ms");

	}

	private static void loadconfig() {
		// Look at the props file and find the inbox
		log.trace("loading properties...");
		props = new Properties();
		try {
			props.load(new BufferedInputStream(new FileInputStream(
					"pdfutil.properties")));

			// Look at inbox and see if there are any ctl files.
			inbox = new File(props.getProperty("InboxDir"));
			outbox = new File(props.getProperty("OutboxDir"));
			tempdir = new File(props.getProperty("TempDir"));
			gc_collect_rows = Integer.parseInt(props
					.getProperty("GcCollectRows"));
			// batch_update_max_rows = Integer.parseInt(props
			// .getProperty("BatchUpdateMaxRows"));
			log.trace("creating temp & outbox directories if doesn't exist...");

			tempdir.mkdirs(); // Ensure that the temp dir exists.
			outbox.mkdirs(); // Ensure that outbox exists.

			// Load the start and stop rows.
			StartDataRow = Integer.parseInt(props.getProperty("CtlStartRow"));
			MaxDataRow = Integer.parseInt(props.getProperty("CtlProcessRows"));

			// Get the ps2pdf command string
			PS2PDF_cmd = props.getProperty("PS2PDF_cmd");
			PS2PDF_ext = props.getProperty("PS2PDF_ext");

			log.trace("reading backer properties...");

			// get the Backers File Path
			allLanguageBacker = props.getProperty("AllLanguagesBacker");
			englishBacker = props.getProperty("EnglishBacker");
			englishAndSpanishBacker = props
					.getProperty("EnglishAndSpanishBacker");
			englishAndChineseBacker = props
					.getProperty("EnglishAndChineseBacker");

			log.trace("Loading all backers in memory...");

			fis = new FileInputStream(allLanguageBacker);
			allLanguageBackerBytes = new byte[fis.available()];
			fis.read(allLanguageBackerBytes);

			fis = new FileInputStream(englishBacker);
			englishBackerBytes = new byte[fis.available()];
			fis.read(englishBackerBytes);

			fis = new FileInputStream(englishAndSpanishBacker);
			englishAndSpanishBackerBytes = new byte[fis.available()];
			fis.read(englishAndSpanishBackerBytes);

			fis = new FileInputStream(englishAndChineseBacker);
			englishAndChineseBackerBytes = new byte[fis.available()];
			fis.read(englishAndChineseBackerBytes);
			log.trace("Loadconfig done...");

		} catch (Exception e) {
			e.printStackTrace();
			log.fatal(e.getStackTrace());
		}

	}

	public static void process_file(File pdfFile, File ctlFile)
			throws Exception {
		log.trace("Started processing file...");
		HashMap tokens = null;
		// System.out.println("PDF[" + pdfFile.getName() + "] CTL[" +
		// ctlFile.getName()+"]");

		System.out.println("CTL FILE Location::" + ctlFile.getAbsolutePath());
		// Load ctl file to parse.
		fwp.closeFile(); // Close any old handles and reopen with Header Skipped
		log.trace("Reloading control file with header skipped...");
		fwp.loadFile(ctlFile.getAbsolutePath(), 1);

		// Convert the source file to compressed format (avoid embedded fonts
		// etc).
		// Simplest way is to convert to print format and back to PDF.
		File psFile = new File(tempdir.getAbsolutePath() + sep
				+ pdfFile.getName() + PS2PDF_ext);

		log.debug("Checking if ps2pdf " + psFile + " exists?...");
		if (psFile.exists()) {
			log.debug("ps2pdf " + psFile
					+ " exists, skipping ps2pdf conversion...");
		} else {
			log.debug("Starting to run ps2pdf " + psFile + "...");
			ps2pdf(pdfFile, psFile);
			log.trace("ps2pdf conversion done...");
		}

		// Load the PDF file to be split and read the bookmarks from the file.
		try {
			log.trace("Creating PDFSplitter object...");
			pdfs = new PDFSplitter();

			// Now load the psFile to cut.
			// pdfs.closeSourceFile();
			log
					.debug("Loading ps2pdf file " + psFile.getAbsolutePath()
							+ "...");
			pdfs.loadSourceFile(psFile.getAbsolutePath());
			log.debug("Reading bookmarks from pdf file...");
			pdfs.readBookmarks();
			log.trace("Reading bookmarks done...");
		} catch (Exception e) {
			log.fatal(e.getStackTrace());
		}
		long startTime = System.currentTimeMillis();
		long endTime = 0;
		// Initialize Rows Processed
		int NoOfRowsProcessed = 0;

		// Advance the number of rows indicated by the StartRow.
		log.trace("Reading data from control file...");
		for (int i = 0; i < StartDataRow; i++) {
			fwp.readDataLine(); // throw away the data.
			++CurDataRow;
		}
		DataPersister dataPersister = new DataPersister();
		// Start extracting from Source PDF based on CTL Data
		log.debug("Loading control file in memory...");
		while ((tokens = fwp.readDataLine()) != null) {
			tokens = fwp.trimHashMap(tokens);
			String eidKey = (String) tokens.get("EID_KEY");
			fwp.addRow(eidKey, tokens);

			// Stop processing if Row Count is exceeded.
			if (MaxDataRow == -1) {
				// Process all rows.
			} else {
				if (!(MaxDataRow >= NoOfRowsProcessed)) {
					// If processing range is completed; stop.
					// This is so that we can run multiple instances to
					// parallelize processing.
					break;
				}
			}
		}
		log.trace("Loading control file in memory done...");

		// int numpgs = pdfs.getReader().getNumberOfPages();
		ByteArrayOutputStream baos = null;
		byte[] backerBytes = null;
		// Step 1 - create a document object.
		Document document = null;// new
		// Document(pdfs.getReader().getPageSizeWithRotation(1));

		log.debug("Reading bookmarks and cutting into individual bills...");
		String bookmarkkey = null;
		for (Integer intkey : pdfs.getBookmarkHM().keySet()) {
			bookmarkkey = intkey.toString();
			// Process single record
			if (processeidkey != null && !processeidkey.equals(bookmarkkey)) {
				continue;
			}
			log.debug("Processing bookmark " + bookmarkkey + "...");
			// int keyInt = Integer.parseInt(bookmarkkey);
			document = new Document(pdfs.getReader().getPageSizeWithRotation(1));
			log.trace("Looking control file data record...");
			BookmarkObject bo = pdfs.getBookmarkHMValues(intkey);
			tokens = fwp.getRow(bookmarkkey);
			String mrnNumber = (String) tokens.get("MRN_NUM");
			String mrnPrefix = (String) tokens.get("MRN_PREFIX");
			String fullName = (String) tokens.get("FULL_NAME");

			if (dataPersister.wasSkippedLastTime(mrnNumber, mrnPrefix,
					fullName, processSkippedRecords, processOnlyAllowedSpecChars,
					processOnlyMorethanOne)) {
				try {
					baos = new ByteArrayOutputStream();
					BufferedOutputStream bos = new BufferedOutputStream(baos);

					// Step 2: we create a writer that listens to the document
					PdfCopy writer = new PdfCopy(document, bos);

					// Step 3: Open document for write.
					document.open();

					log.trace("Splitting file from page " + bo.pageFrom + " - "
							+ bo.pageTo + "...");
					for (int i = bo.pageFrom; i <= bo.pageTo; i++) {
						PdfImportedPage page = writer.getImportedPage(pdfs
								.getReader(), i);
						writer.addPage(page);
					}
					log.trace("Splitting file done...");

					// Step 5: Close the document
					document.close();
					bos.flush();
					bos.close();
					document = null;
					log.trace("Loading split file as byte array...");
					billBytes = baos.toByteArray();
				} catch (Exception e) {
					log.info("The Exception is ::" + e);
					throw e;
				}
				baos = null;

				if (NoOfRowsProcessed != 0
						&& NoOfRowsProcessed % gc_collect_rows == 0) {
					log.info("Running garbage collect, records processed: "
							+ (NoOfRowsProcessed));
				}

				// if(NoOfRowsProcessed !=0 &&
				// NoOfRowsProcessed % batch_update_max_rows ==0){
				// log.info("Running Batch Update ");
				// dataPersister.executeBatchUpdate();
				// dataPersister.clearUpdateList();
				// }
				// if(NoOfRowsProcessed == 3000){
				// log.info("Last Running Batch Update ");
				// dataPersister.executeBatchUpdate();
				// dataPersister.clearUpdateList();
				// System.exit(0);
				// }

				NoOfRowsProcessed++;
				log.debug("Processing bill record " + NoOfRowsProcessed);

				// Warning - tokens are still strings.
				// tokens = fwp.trimHashMap(tokens); // Check if PDF has trimmed
				// values

				try {
					// Construct the key from the CTL file.
					String key = (String) tokens.get("EID_KEY") + ": "
							+ (String) tokens.get("ACCT_NUM");
					// pdfs.printBookmark(key); //Check if bookmark exists

					Formatter fmt = new Formatter();
					fmt.format("%09d", Integer.parseInt((String) tokens
							.get("EID_KEY")));
					String output_file_name = tempdir.getAbsolutePath() + sep
							+ (String) HeaderTokens.get("ST_DTE")
							+ "-"
							+ // Statement
							// Date
							fmt.toString() + "-"
							+ // EID Key
							(String) tokens.get("MRN_NUM")
							+ // ACCT NUM
							"-" + (String) tokens.get("MRN_PREFIX") + "-"
							+ (String) tokens.get("FULL_NAME") + ".pdf";

					log.debug("Output file name based on key "
							+ output_file_name);
					// Use the index range from bookmark in Orig file to cut the
					// PS2PDF file.
					// byte[] billBytes = pdfs.extractPgsUsingPageIndex(pdfs
					// .getBookmarkStartIndex(key), pdfs
					// .getBookmarkStopIndex(key));

					// get the Network Model Name
					log.debug("Checking backer to be applied...");
					String networkModel = (String) tokens.get("NETWORK_MODEL");
					String productSubType = (String) tokens
							.get("PRODUCT_SUBTYPE");
					String language = (String) tokens.get("LANGUAGE");

					// Add the Backers
					if (networkModel.equals("EPO")
							&& (productSubType.trim().equals("CSI") || productSubType
									.trim().equals("CSI HSA"))) {
						log.trace("Applying allLanguageBacker...");
						backerBytes = allLanguageBackerBytes;
					} else if (language.equals("SPANISH")) {
						log.trace("Applying englishAndSpanishBacker...");
						backerBytes = englishAndSpanishBackerBytes;
					} else if (language.equals("CHINESE")) {
						log.trace("Applying englishAndChineseBacker...");
						backerBytes = englishAndChineseBackerBytes;
					} else {
						// Setting english as the default backer
						log.trace("Applying englishBacker...");
						backerBytes = englishBackerBytes;
					}

					// Add watermark of preprinted stationary.

					log.debug("Applying watermark...");
					long wt1 = System.currentTimeMillis();
					billBytes = pdfWatermark.pdfWatermark(billBytes);
					long wt2 = System.currentTimeMillis();
					System.out.println("@@@@Watermark done in :"+(wt2-wt1)+" ms");
					log.trace("Watermark done...");
					// Get FSM save directory
					String save_path = fsm.getSavePath(output_file_name,
							monthsep);
					log.debug("FileSystemManager save path:" + save_path);
					
					//check for the missed records
					if(findMissedRecords){
						if(dataPersister.isBillMissed(save_path)){
							log.info("Record Got Missed in the Last Import Process:MRN="+(String) tokens.get("MRN_NUM")+",NAME="+(String) tokens.get("FULL_NAME")+"&MRN PREFIX="+(String) tokens.get("MRN_PREFIX")+"::File Saved Path is :" + save_path);
						}
					}

					if (backerBytes != null) {
						log
								.debug("Applying backer and getting it back as byte array...");
						long bt1 = System.currentTimeMillis();
						billBytes = pdfConcatenator.concatPDFs(billBytes,
								backerBytes, false);
						long bt2 = System.currentTimeMillis();
						System.out.println("@@@@Backer done in :"+(bt2-bt1)+" ms");
						log.trace("Backer done...");
					}

					/* Encrypt and save to the FSM directory */
					log
							.debug("Encrypting and saving byte array in FileSystemManager path..."
									+ save_path);
					long st1 = System.currentTimeMillis();
					if (saveToFsm) {
						if (encryption) {
							fsm.encryptAndSave(billBytes, save_path);
						}else{
							fsm.save(billBytes, save_path);
						}
					}
					long st2 = System.currentTimeMillis();
					System.out.println("@@@@FSM Save in :"+(st2-st1)+" ms");
					log.trace("Encryption and saving done...");
					

					long dbt1 = System.currentTimeMillis();
					log.debug("Updating Database...");
					RecordReference rr = new RecordReference();
					rr.setAcct_num((String) tokens.get("ACCT_NUM"));
					rr.setCrypto_key(fsm.getKey());
					rr.setEid_key((String) tokens.get("EID_KEY"));
					rr.setSave_path(save_path);
					rr.setSt_dte((String) HeaderTokens.get("ST_DTE"));
					rr.setRowTokens(tokens);
					
					// Save the Record Reference.
					// This would be updated to save to database
					log.debug("Saving record in database...");
					if (saveToDb) {
						dataPersister.saveRecordReference(rr);
					}
					long dbt2 = System.currentTimeMillis();
					System.out.println("@@@@DB Save in :"+(dbt2-dbt1)+" ms");
					log.trace("Record saving done...");

					/* Saving in mongo database */
					long sqlt1 = System.currentTimeMillis();
					MongoPersister.save(rr, billBytes);
					long sqlt2 = System.currentTimeMillis();
					System.out.println("@@@@Mongo Save in :"+(sqlt2-sqlt1)+" ms");
		    	    /* done saving */

					rr = null;
					billBytes = null;
					// rrlist.add(rr);
					endTime = System.currentTimeMillis();
					log.info("Index#" + bookmarkkey + ", Record "
							+ NoOfRowsProcessed + " processed in "
							+ (endTime - startTime) + " ms");
					// System.out.println("Family-MRN_NUM:" +
					// tokens.get("MRN_NUM")
					// + " - Time taken:" + (endTime - startTime) + " ms");
					// System.out.println("-------------------------------------------------------------");
					startTime = System.currentTimeMillis();
				} catch (Exception e) {
					log.error(e.getStackTrace());
					throw e;
				}
			}
		}
		// log.info("Running Bacth Update ");
		// dataPersister.executeBatchUpdate();
		// dataPersister.clearUpdateList();
		// dataPersister.closeConnections();
		dataPersister = null;
		// Update the Email Addresses
		boolean emailsUploaded = EmailUpdator.updateEmails();
		// Send the mail to all the Users
//		Thread thread = new Thread(new SendMailClient());
//		thread.start();
//		log.info("Total Records processed " + NoOfRowsProcessed
//				+ ", Total time taken " + (endTime - appStartTime) + " ms");
	}

	public static String getFileNameRoot(String fileName) {
		File tmpFile = new File(fileName);
		tmpFile.getName();
		int whereDot = tmpFile.getName().lastIndexOf('.');
		if (0 < whereDot && whereDot <= tmpFile.getName().length() - 2) {
			return tmpFile.getName().substring(0, whereDot);
			// extension = filename.substring(whereDot+1);
		}
		return "";
	}

	/**
	 * Converts a PDF file to an optimized representation. Internally uses
	 * filesystem specific PS2PDF commands as provided in the pdfutil.properties
	 * file to print the file to a PS and convert it back to PDF. Note that this
	 * approach may potentially strip other metadata informaiton present in the
	 * PDF file (notably bookmarks, annotations etc) which are not in the
	 * default "PRINT = Y" configuration.
	 * 
	 * @param pdfFile
	 *            source file in PDF
	 * @param psFile
	 *            destination file in PS/PDF
	 * @return the image at the specified URL
	 * @see KEBP
	 */
	public static boolean ps2pdf(File pdfFile, File psFile) {
		boolean status = false;
		String s = null;
		try {

			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			Process p = Runtime.getRuntime().exec(
					PS2PDF_cmd + " " + pdfFile.getAbsolutePath() + " "
							+ psFile.getAbsolutePath());
			// Wait for the process to finish
			try {
				p.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (false) { // Turn this on if you need to see the output.

				BufferedReader stdInput = new BufferedReader(
						new InputStreamReader(p.getInputStream()));
				BufferedReader stdError = new BufferedReader(
						new InputStreamReader(p.getErrorStream()));

				// read the output from the command
				System.out.println("STDOUT:\n");
				while ((s = stdInput.readLine()) != null) {
					System.out.println(s);
				}
				// read any errors from the attempted command
				System.out.println("STDERR:\n");
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
				}
			}
			status = true;

		} catch (java.io.IOException e) {
			e.printStackTrace();
			status = false;
		}
		return status;
	}
}
