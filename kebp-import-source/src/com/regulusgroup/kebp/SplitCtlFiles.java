package com.regulusgroup.kebp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Properties;
import org.apache.log4j.Logger;

public class SplitCtlFiles {
    private static final Logger logger = Logger.getLogger(SplitCtlFiles.class);
	/**
	 * @param args
	 */
	public void splitCtlFiles() {
		Properties props = new Properties();
		try {
			props.load(new BufferedInputStream(new FileInputStream(
					"pdfutil.properties")));
			String unzipDir = props.getProperty("UnzipDir");
			String inbox = props.getProperty("InboxDir");
			int billsToCut = Integer.parseInt(props.getProperty("billsToCut"));

			File dir = new File(unzipDir);

			FilenameFilter filter = new FilenameFilter() {
			    public boolean accept(File dir, String name) {
			        return name.endsWith(".ctl");
			    }
			};			
			File[] children = dir.listFiles(filter);
			if (children == null) {
			    System.out.println("Not a directory");
				// Either dir does not exist or is not a directory
			} else {
			    for (int i=0; i<children.length; i++) {
			        // Get filename of file or directory
			    	String line = null;
			    	int lineNum = 0;
			    	int fileCounter = 1;
			    	String initialCtlFileName = children[i].getName().substring(0, children[i].getName().lastIndexOf(".ctl"));
			    	String ctlFileName = inbox + "/" + initialCtlFileName + "." + new DecimalFormat("00").format(fileCounter)+".ctl";
			    	String pdfSrcFileName = unzipDir + "/" + initialCtlFileName + "." + new DecimalFormat("00").format(fileCounter)+".pdf";
			    	String pdfDestFileName = inbox + "/" + initialCtlFileName + "." + new DecimalFormat("00").format(fileCounter)+".pdf";
			    	BufferedReader br = new BufferedReader(new FileReader(children[i]));
			    	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(ctlFileName)));
		    		String headerLine = br.readLine();
		    		
		    		
		    		// Change file name in ctl file 
		    		bw.write(headerLine);
		    		bw.newLine();
		    		
			    	do{
			    		lineNum++;
			    		line = br.readLine();

			    		if(line!=null){
				    		if(lineNum>billsToCut){
				    			lineNum=1;
				    			bw.flush();
				    			bw.close();
				    			copyfile(pdfSrcFileName, pdfDestFileName);
				    			fileCounter++;
				    			ctlFileName = inbox + "/" + initialCtlFileName + "." + new DecimalFormat("00").format(fileCounter)+".ctl";
						    	pdfSrcFileName = unzipDir + "/" + initialCtlFileName + "." + new DecimalFormat("00").format(fileCounter)+".pdf";
						    	pdfDestFileName = inbox + "/" + initialCtlFileName + "." + new DecimalFormat("00").format(fileCounter)+".pdf";
				    			bw = new BufferedWriter(new FileWriter(new File(ctlFileName)));
					    		bw.write(headerLine);
					    		bw.newLine();
				    		}
				    		bw.write(line);
				    		bw.newLine();
			    		}
			    		//System.out.println("Line No:"+lineNum+" "+str);
			    	}while(line!=null);
			    	bw.flush();
	    			bw.close();
	    			copyfile(pdfSrcFileName, pdfDestFileName);
			    }
			    
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			// Clean up
		}
	}
	
	private void copyfile(String srFile, String dtFile){
	    try{
	      File f1 = new File(srFile);
	      File f2 = new File(dtFile);
	      InputStream in = new FileInputStream(f1);
	      
	      //For Append the file.
//	      OutputStream out = new FileOutputStream(f2,true);

	      //For Overwrite the file.
	      OutputStream out = new FileOutputStream(f2);

	      byte[] buf = new byte[1024];
	      int len;
	      while ((len = in.read(buf)) > 0){
	        out.write(buf, 0, len);
	      }
	      in.close();
	      out.close();
	      System.out.println("File copied.");
	    }
	    catch(FileNotFoundException ex){
	      System.out.println(ex.getMessage() + " in the specified directory.");
	      System.exit(0);
	    }
	    catch(IOException e){
	      System.out.println(e.getMessage());      
	    }
	  }	

}
