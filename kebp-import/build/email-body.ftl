Your online statement is ready for viewing. To see your statement or change paperless preferences, log on to http://kp.org/viewbill.

Please do not reply to this email. This notice is automatically generated. 

Thank you for using our online services.

This e-mail was sent to you by Kaiser Permanente. To ensure that future announcements are delivered to your inbox (not your bulk folder), please add kpviewbill@regulusgroup.com to your address book.

Our Web site privacy statement: https://members.kaiserpermanente.org/kpweb/privacystate/entrypage.do


::${uuid}::








